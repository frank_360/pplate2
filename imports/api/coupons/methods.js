import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Coupons } from './coupons.js';
import { createClient } from 'contentful';
import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin';

export const checkCoupon = new ValidatedMethod({
  name: 'coupons.checkCoupon',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    code: { type: String },
  }).validator(),
  run({ code }) {
    if (Meteor.isServer) {
      const coupon = Coupons.findOne({ code, used: false, active: true });
      if (coupon) return true;
      return false;
    }
  },
});

export const checkPromo = new ValidatedMethod({
  name: 'coupons.checkPromo',
  validate: new SimpleSchema({
    code: { type: String },
    membership: { type: String }
  }).validator(),
  async run({ code, membership }) {
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    const entries = await client.getEntries({
      content_type: 'coupons',
      'fields.code': code,
    });
    if (membership.includes('1') && code.charAt(0) !== 'M') {
      return false;
    } else if (membership.includes('3') && code.charAt(0) !== '3') {
      return false;
    } else if (membership.includes('6') && code.charAt(0) !== '6') {
      return false;
    }

    return entries && entries.total;
  },
});

export const createCoupon = new ValidatedMethod({
  name: 'coupons.createCoupon',
  validate: new SimpleSchema({
    profile: { type: Object, blackbox: true },
    gifter: { type: Object, blackbox: true },
    email: { type: String },
    membership: { type: Object, blackbox: true },
  }).validator(),
  run({ profile, gifter, email, membership }) {
    if (Meteor.isServer) {
      const code = Math.random().toString(36).substr(2, 6).toUpperCase();
      console.log('Code: ', code);
      Coupons.insert({
        code,
        email,
        profile,
        gifter,
        membership,
      });
      return code;
    }
  },
});

export const getCoupon = new ValidatedMethod({
  name: 'coupons.getCoupon',
  validate: new SimpleSchema({
    code: { type: String },
  }).validator(),
  run({ code }) {
    if (Meteor.isServer) {
      return Coupons.findOne({ code, used: false });
    }
  },
});

export const purchaseGift = new ValidatedMethod({
  name: 'coupons.purchaseGift',
  validate: new SimpleSchema({
    token: { type: Object, blackbox: true },
    amount: { type: Number },
    code: { type: String },
  }).validator(),
  run({ token, amount, code }) {
    if (Meteor.isServer) {
      console.log('purchaseGift');
      const coupon = Coupons.findOne({ code });
      import stripePackage from 'stripe';
      const stripe = stripePackage(Meteor.settings.STRIPE_SECRET_KEY);

      // make payment
      try {
        if (amount) {
          const response = stripe.customers.create({
            amount,
            currency: 'usd',
            source: token.id,
            description: `Charge for gift for ${coupon.email} from ${coupon.gifter.email}`,
            receipt_email: coupon.gifter.email,
          }, (err, customer) => {
            if (err) throw new Error('cant charge', err);

            stripe.charges.create({
              amount,
              currency: 'usd',
              capture: true,
              description: `Pregnancy Plate Plan ${membership.title}`,
              customer: null,
            }, (err, charge) => {
              Method.users.update();
            });
          });
          Coupons.update({ code }, { $set: {
            active: true,
            charge_id: response.id,
          } });
        } else {
          Coupons.update({ code }, { $set: {
            active: true,
            charge_id: 'free',
          } });
        }
        // send email
        this.unblock();
        const SendinBlueApi = require('sib-api-v3-sdk');
        const defaultClient = SendinBlueApi.ApiClient.instance;
        const apiKey = defaultClient.authentications['api-key'];
        apiKey.apiKey = Meteor.settings.SEND_IN_BLUE;
        const api = new SendinBlueApi.SMTPApi();
        const sendEmail = new SendinBlueApi.SendEmail();
        // send to gifter
        sendEmail.emailTo = [coupon.gifter.email];
        sendEmail.attributes = { EMAIL: coupon.email };
        api.sendTemplate(1, sendEmail).then((res, rej) => {
          if (rej) console.log(rej);
        });
        // send to gift recipient
        sendEmail.emailTo = [coupon.email];
        sendEmail.attributes = {
          NAME: `${coupon.profile.firstName} ${coupon.profile.lastName}`,
          FRIEND: `${coupon.gifter.firstName} ${coupon.gifter.lastName}`,
          COUPON: coupon.code,
        };
        api.sendTemplate(2, sendEmail).then((res, rej) => {
          if (rej) console.log(rej);
        });
      } catch (e) {
        throw new Meteor.Error('cant charge', e);
      }
    }
  },
});

export const sendAccountEmail = new ValidatedMethod({
  name: 'profile.sendAccountEmail',
  validate: new SimpleSchema({
    email: { type: String },
    profile: { type: Object, blackbox: true },
  }).validator(),
  run({ email, profile }) {
    if(Meteor.isServer) {
      // send email
      this.unblock();
      const SendinBlueApi = require('sib-api-v3-sdk');
      const defaultClient = SendinBlueApi.ApiClient.instance;
      const apiKey = defaultClient.authentications['api-key'];
      apiKey.apiKey = Meteor.settings.SEND_IN_BLUE;
      const api = new SendinBlueApi.SMTPApi();
      const sendEmail = new SendinBlueApi.SendEmail();
      // send to gift recipient
      sendEmail.emailTo = [email];
      sendEmail.attributes = {
        NAME: `${profile.firstName} ${profile.lastName}`,
        USERNAME: `${email}`,
      };
      api.sendTemplate(3, sendEmail).then((res, rej) => {
        if (rej) console.log(rej);
      });
    }
  },
});

export const claimGift = new ValidatedMethod({
  name: 'coupons.claimGift',
  validate: new SimpleSchema({
    code: { type: String },
  }).validator(),
  run({ code }) {
    if (Meteor.isServer) {
      const coupon = Coupons.findOne({ code });
      Meteor.users.update(Meteor.user()._id, {
        $set: {
          charge_id: coupon.charge_id,
        },
      });
      Coupons.update({ code }, {
        $set: {
          used: true,
        },
      });
    }
  },
});
