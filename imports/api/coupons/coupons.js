import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Coupons = new Mongo.Collection('coupons');

const Schema = {};

Schema.UserProfile = new SimpleSchema({
  firstName: {
    type: String,
    optional: true,
  },
  lastName: {
    type: String,
    optional: true,
  },
  couponCode: {
    type: String,
    optional: true,
  },
  dueDate: {
    type: Date,
    optional: true,
  },
  firstTime: {
    type: Boolean,
    optional: true,
  },
  membership: {
    type: Object,
    optional: true,
  },
});

Schema.GifterProfile = new SimpleSchema({
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  email: {
    type: String,
  },
});

Schema.Coupons = new SimpleSchema({
  code: {
    type: String,
    optional: true,
  },
  active: {
    type: Boolean,
    defaultValue: false,
  },
  used: {
    type: Boolean,
    defaultValue: false,
  },
  profile: {
    type: Schema.UserProfile,
  },
  gifter: {
    type: Schema.GifterProfile,
  },
  email: {
    type: String,
  },
  charge_id: {
    type: String,
    optional: true,
  },
});

Coupons.attachSchema(Schema.Coupons);
