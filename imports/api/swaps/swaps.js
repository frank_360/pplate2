import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Swaps = new Mongo.Collection('swaps');

const Schema = {};

Schema.Swaps = new SimpleSchema({
  userId: {
    type: String,
  },
  week: {
    type: Number,
  },
  day: {
    type: String,
    allowedValues: ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday'],
  },
  recipeId: {
    type: String,
  },
});

Swaps.attachSchema(Schema.Swaps);
