import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Swaps } from '../swaps.js';

Meteor.publish('swaps.myWeeklySwaps', function allRatings(week) {
  check(week, Number);
  if (!this.userId) return [];
  return Swaps.find({ userId: this.userId, week });
});
