import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { createClient } from 'contentful';
import { Swaps } from './swaps.js';
import _ from 'underscore';

export const newSwap = new ValidatedMethod({
  name: 'swap.newSwap',
  validate: new SimpleSchema({
    week: { type: Number },
    day: { type: String },
    recipeId: { type: String },
  }).validator(),
  run({ week, day, recipeId }) {
    Swaps.upsert({ week, day, userId: Meteor.userId() }, {
      $set: {
        userId: Meteor.userId(),
        week,
        day,
        recipeId,
      },
    });
  },
});

export const deleteSwap = new ValidatedMethod({
  name: 'swap.deleteSwap',
  validate: new SimpleSchema({
    week: { type: Number },
    day: { type: String },
  }).validator(),
  run({ week, day }) {
    Swaps.remove({ week, day, userId: Meteor.userId() });
  },
});

export const getShoppingList = new ValidatedMethod({
  name: 'swap.getShoppingList',
  validate: new SimpleSchema({
    weekNumber: { type: Number },
  }).validator(),
  async run({ weekNumber }) {
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    const entries = await client.getEntries({
      content_type: 'week',
      'fields.number': weekNumber,
      include: 1,
    });
    if (entries.items && entries.items[0]) {
      const week = entries.items[0].fields;
      const recipes = {
        sunday: week.sunday ? week.sunday.fields : {},
        monday: week.monday ? week.monday.fields : {},
        tuesday: week.tuesday ? week.tuesday.fields : {},
        wednesday: week.wednesday ? week.wednesday.fields : {},
        thursday: week.thursday ? week.thursday.fields : {},
      };
      const swaps = Swaps.find({ userId: Meteor.userId() }).fetch();
      const list = [];
      _.each(recipes, (r, key) => {
        const swap = _.findWhere(swaps, { day: key });
        if (swap) list.push({ recipeId: swap.recipeId, day: key });
      });
      if (list.length > 0) {
        const sEntries = await client.getEntries({
          content_type: 'recipe',
          'sys.id[in]': _.pluck(list, 'recipeId').toString(),
        });
        if (sEntries.items) {
          _.each(swaps, (e) => {
            const recipe = _.find(sEntries.items, item => item.sys.id === e.recipeId);
            if (recipe) {
              const day = e.day;
              recipe.fields.swap = true;
              recipes[day] = recipe.fields;
            }
          });
        }
      }
      const ingredients = [];
      _.each(recipes, r => ingredients.push(...r.ingredients));
      const objIngredients = {}
      _.each(ingredients, (i) => {
        if (!i) return false;
        const iMap = i.split(/@(.+)/);
        if (!iMap[0]) return false;
        const key = iMap[1].trim();
        if (objIngredients[key]) objIngredients[key] = `${objIngredients[key]}, ${iMap[0].trim()}`;
        else objIngredients[key] = iMap[0].trim();
      });
      return objIngredients;
    } return {};
  },
});
