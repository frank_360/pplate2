import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Ratings } from './ratings.js';
import { createClient } from 'contentful';
import _ from 'underscore';

export const newRating = new ValidatedMethod({
  name: 'ratings.newRating',
  validate: new SimpleSchema({
    recipeId: { type: String },
    stars: { type: Number },
  }).validator(),
  run({ recipeId, stars }) {
    if (Meteor.isServer) {
      Ratings.upsert({ userId: Meteor.userId(), recipeId }, {
        $set: {
          userId: Meteor.userId(),
          recipeId,
          stars,
        },
      });
    }
  },
});

export const madeThisToggle = new ValidatedMethod({
  name: 'ratings.madeThisToggle',
  validate: new SimpleSchema({
    recipeId: { type: String },
    made: { type: Boolean },
  }).validator(),
  run({ recipeId, made }) {
    if (Meteor.isServer) {
      if (made) {
        Meteor.users.update(Meteor.userId(), {
          $pull: {
            madeRecipes: recipeId,
          },
        });
      } else {
        Meteor.users.update(Meteor.userId(), {
          $addToSet: {
            madeRecipes: recipeId,
          },
        });
      }
    }
  },
});

export const isMade = new ValidatedMethod({
  name: 'ratings.isMade',
  validate: new SimpleSchema({
    recipeId: { type: String },
  }).validator(),
  run({ recipeId }) {
    if (Meteor.isServer) {
      const madeRecipes = Meteor.user().madeRecipes || [];
      return madeRecipes && _.contains(madeRecipes, recipeId);
    }
  },
});


export const getRecipeStats = new ValidatedMethod({
  name: 'ratings.getRecipeStats',
  validate: null,
  async run() {
    if (Meteor.user().admin && Meteor.isServer) {
      const client = createClient({
        space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
        accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
      });
      const entries = await client.getEntries({
        content_type: 'recipe',
      });
      const entryMap = await Promise.all(_.map(entries.items, async (e) => {
        const title = e.fields.title;
        const timesMade = Meteor.users.find({ madeRecipes: e.sys.id }).count();
        const avgRatingArray = await Ratings.rawCollection().aggregate([
          {
            $match: {
              recipeId: e.sys.id,
            },
          },
          {
            $group: {
              _id: null,
              average: { $avg: '$stars' },
            },
          },
        ]).toArray();
        let avgRating = 0;
        if (avgRatingArray[0]) avgRating = avgRatingArray[0].average;
        return {
          title,
          timesMade,
          avgRating,
        };
      }));
      return entryMap;
    }
  },
});
