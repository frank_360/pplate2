import { Meteor } from 'meteor/meteor';
import { Ratings } from '../ratings.js';

Meteor.publish('ratings.myRatings', function allRatings() {
  if (!this.userId) return [];
  return Ratings.find({ userId: this.userId });
});
