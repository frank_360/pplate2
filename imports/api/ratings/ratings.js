import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Ratings = new Mongo.Collection('ratings');

const Schema = {};

Schema.Ratings = new SimpleSchema({
  userId: {
    type: String,
  },
  recipeId: {
    type: String,
  },
  stars: {
    type: Number,
    allowedValues: [1, 2, 3, 4, 5],
  },
});

Ratings.attachSchema(Schema.Ratings);
