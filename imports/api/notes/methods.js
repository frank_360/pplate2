import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Notes } from './notes.js';

export const updateNotes = new ValidatedMethod({
  name: 'notes.updateNotes',
  validate: new SimpleSchema({
    recipeId: { type: String },
    text: { type: String },
  }).validator(),
  run({ recipeId, text }) {
    if (Meteor.isServer) {
      Notes.upsert({ userId: Meteor.userId(), recipeId }, {
        $set: {
          userId: Meteor.userId(),
          recipeId,
          text,
        },
      });
    }
  },
});

export const getNote = new ValidatedMethod({
  name: 'notes.getNote',
  validate: new SimpleSchema({
    recipeId: { type: String },
  }).validator(),
  run({ recipeId }) {
    if (Meteor.isServer) {
      return Notes.findOne({ userId: Meteor.userId(), recipeId });
    }
  },
});
