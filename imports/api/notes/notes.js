import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Notes = new Mongo.Collection('notes');

const Schema = {};

Schema.Notes = new SimpleSchema({
  userId: {
    type: String,
  },
  recipeId: {
    type: String,
  },
  text: {
    type: String,
  },
});

Notes.attachSchema(Schema.Notes);
