import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const Schema = {};

Schema.UserProfile = new SimpleSchema({
  firstName: {
    type: String,
    optional: true,
  },
  lastName: {
    type: String,
    optional: true,
  },
  couponCode: {
    type: String,
    optional: true,
  },
  dueDate: {
    type: Date,
    optional: true,
  },
  firstTime: {
    type: Boolean,
    optional: true,
  },
  membership: {
    type: Object,
    optional: true,
  },
});

Schema.User = new SimpleSchema({
  emails: {
    type: Array,
    optional: true,
  },
  'emails.$': {
    type: Object,
  },
  'emails.$.address': {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
  'emails.$.verified': {
    type: Boolean,
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true,
  },
  profile: {
    type: Schema.UserProfile,
    optional: true,
    blackbox: true,
  },
  createdAt: {
    type: Date,
    defaultValue: new Date(),
  },
  socialOnboarded: {
    type: Boolean,
    optional: true,
  },
  madeRecipes: {
    type: Array,
    optional: true,
  },
  'madeRecipes.$': {
    type: String,
  },
  charge_id: {
    type: String,
    optional: true,
  },
  admin: {
    type: Boolean,
    optional: true,
  },
  expDate: {
    type: String,
    optional: true,
  },
  status: {
    type: String,
    optional: false,
  },
});

Meteor.users.attachSchema(Schema.User);

Meteor.users.deny({
  update() { return false; },
});

Meteor.users.allow({
  remove() { return true; },
  update() { return true; }
});
