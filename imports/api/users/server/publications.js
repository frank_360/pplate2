import { Meteor } from 'meteor/meteor';

Meteor.publish('users.myData', function privateUser() {
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: this.userId });
});

Meteor.publish('users.membership', function privateUser() {
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: this.userId }, { fields: {
    membership: 1,
    charge_id: 1,
    expDate: 1,
    status: 1,
  } });
});

Meteor.publish('users.allUsers', function privateUser() {
  if (!this.userId) return [];
  return Meteor.users.find({});
});