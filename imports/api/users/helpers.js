import moment from 'moment';

// Configuration agnostic user helpers
export function getFullName(user) {
  if (user.services && user.services.google) {
    return user.services.google.name;
  } else if (user.services && user.services.facebook) {
    return user.services.facebook.name;
  } else if (user.profile.firstName && user.profile.lastName) {
    return `${user.profile.firstName} ${user.profile.lastName}`;
  }
  return '';
}

export function getFirstName(user) {
  if (user.services && user.services.google) {
    return user.services.google.given_name;
  } else if (user.services && user.services.facebook) {
    return user.services.facebook.first_name;
  } else if (user.profile.firstName) {
    return user.profile.firstName;
  }
  return '';
}

export function getEmail(user) {
  if (user.services && user.services.facebook) {
    return user.services.facebook.email;
  }
  if (user.services && user.services.google) {
    return user.services.google.email;
  }
  return user.emails[0].address;
}

export function isSocialAccount(user) {
  if (!user.services) {
    return true;
  }
  return user.services.facebook || user.services.google;
}

export function getWeek(user) {
  if (!user) return false;
  const dueDate = user.profile.dueDate;
  if (!dueDate) return false;
  const weeksUntil = moment(dueDate).diff(moment(), 'weeks');
  const diff = 40 - weeksUntil;
  if (diff > 40) return 40;
  else if (diff < 4) return 4;
  return diff;
}
