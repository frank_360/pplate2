import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { browserHistory } from "react-router";
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { getEmail } from './helpers.js';
import SimpleSchema from 'simpl-schema';
import { HTTP } from 'meteor/http';
import { SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG } from 'constants';

export const myAccount = new ValidatedMethod({
  name: 'users.myAccount',
  validate: null,
  run() {
    if (Meteor.isServer) {
      return Meteor.user();
    }
  },
});

export const updateMembership = new ValidatedMethod({
  name: 'users.updateMembership',
  validate: new SimpleSchema({
    title: { type: String },
    price: { type: Number },
    frequency: { type: String },
  }).validator(),
  run({ title, price, frequency }) {
    if (Meteor.isClient) {
      return Meteor.users.update(Meteor.userId(), {
        $set: {
          'profile.membership.price': price,
          'profile.membership.title': title,
          'profile.membership.frequency': frequency,
        },
      });
    }
  },
});

export const cancelSub = new ValidatedMethod({
  name: 'users.cancelSub',
  validate: new SimpleSchema({
    subId: { type: String },
    user: { type: String },
  }).validator(),
  run({ subId, user }) {
    if (Meteor.isServer) {
      import stripePackage from 'stripe';

      const stripe = stripePackage(Meteor.settings.STRIPE_SECRET_KEY);
      const delSubSync = Meteor.wrapAsync(stripe.subscriptions.del, stripe.subscriptions);

      try {
        if (subId !== 'free') {
          const deletedSub = delSubSync(subId);
          console.log('ran');
          return Meteor.users.update({ _id: user }, {
            $set: {
              status: 'cancelled',
            },
            $unset: {
              charge_id: null,
              'services.password': {},
            },
          });
        }
        return Meteor.users.update({ _id: user }, {
          $set: {
            status: 'cancelled',
          },
          $unset: {
            charge_id: null,
            'services.password': {},
          },
        });
      } catch (e) {
        console.dir(e.message);
        throw new Meteor.Error('cant delete', e);
      }
    }
  },
});

export const purchaseSub = new ValidatedMethod({
  name: 'users.purchaseSub',
  validate: new SimpleSchema({
    token: { type: Object, blackbox: true },
    amount: { type: Number },
    membership: { type: Object, blackbox: true },
    user: { type: Object, blackbox: true },
    upgrade: { type: Boolean },
  }).validator(),
  run({ token, amount, membership, user, upgrade }) {
    if (Meteor.isServer) {
      import stripePackage from 'stripe';

      const stripe = stripePackage(Meteor.settings.STRIPE_SECRET_KEY);
      const switchCase = membership.title.match(/\d+/g)[0];
      let expDate = '';

      const customerDetails = {
        description: `Customer for ${getEmail(user)}`,
        source: token.id,
        email: getEmail(user),
      };

      const chargeDetails = {
        amount,
        currency: 'usd',
        capture: true,
        description: `Pregnancy Plate Plan ${membership.title}`,
        customer: null,
      };

      const subscriptionDetails = {
        items: [
          {
            plan: null,
          },
        ],
      };

      switch (amount) {
        case 0:
          subscriptionDetails.items[0].plan = 'baseSub';
          subscriptionDetails.trial_from_plan = true;
          break;
        case 1800:
          subscriptionDetails.items[0].plan = 'monthly';
          break;
        default:
          subscriptionDetails.items[0].plan = null;
          break;
      }

      if (membership) {
        switch (Number(switchCase)) {
          case 2:
            expDate = new Date(Date.now() + (14 * 24 * 60 * 60 * 1000)).toString();
            break;
          case 1:
            expDate = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000)).toString();
            break;
          case 3:
            expDate = new Date(Date.now() + (90 * 24 * 60 * 60 * 1000)).toString();
            break;
          case 6:
            expDate = new Date(Date.now() + (180 * 24 * 60 * 60 * 1000)).toString();
            break;
          default:
            expDate = null;
        }
      }

      const customerSync = Meteor.wrapAsync(stripe.customers.create, stripe.customers);
      const chargeSync = Meteor.wrapAsync(stripe.charges.create, stripe.charges);
      const subscriptionSync = Meteor.wrapAsync(stripe.subscriptions.create, stripe.subscriptions);

      // make payment
      try {
        if (subscriptionDetails.items[0].plan === 'baseSum' || subscriptionDetails.items[0].plan === 'monthly') {
          const customer = customerSync(customerDetails);

          subscriptionDetails.customer = customer.id;

          const paymentResponse = subscriptionSync(subscriptionDetails);

          if (upgrade) {
            Meteor.users.update({ _id: user._id }, {
              $set: {
                'profile.membership': membership,
                expDate,
                charge_id: paymentResponse.id,
              },
            });
          } else {
            Meteor.users.update(user._id, {
              $set: {
                charge_id: paymentResponse.id,
                status: 'active',
              },
            });
          }
        } else if (subscriptionDetails.items[0].plan === null) {

          const customer = customerSync(customerDetails);
          chargeDetails.customer = customer.id;

          const chargeResponse = chargeSync(chargeDetails);

          if (upgrade) {
            Meteor.users.update({ _id: user._id }, {
              $set: {
                expDate,
                'profile.membership': membership,
                charge_id: chargeResponse.id,
              },
            });
          } else {
            Meteor.users.update(user._id, {
              $set: {
                charge_id: chargeResponse.id,
                status: 'active',
              },
            });
          }
        } else {
          Meteor.users.update(user._id, {
            $set: {
              charge_id: 'free',
              status: 'active',
            },
          });
        }
      } catch (e) {
        console.dir(e);
        throw new Meteor.Error('cant charge', e);
      }
    }
  },
});

export const updateProfileFromSocial = new ValidatedMethod({
  name: 'users.updateProfileFromSocial',
  validate: new SimpleSchema({
    profile: { type: Object, blackbox: true },
  }).validator(),
  run({ profile }) {
    if (Meteor.isServer) {
      return Meteor.users.update(Meteor.userId(), {
        $set: {
          profile,
          socialOnboarded: true,
        },
      });
    }
  },
});

export const updateDueDate = new ValidatedMethod({
  name: 'users.updateDueDate',
  validate: new SimpleSchema({
    date: { type: Date },
  }).validator(),
  run({ date }) {
    if (Meteor.isServer) {
      return Meteor.users.update(Meteor.userId(), {
        $set: {
          'profile.dueDate': date,
        },
      });
    }
  },
});

export const deleteUser = new ValidatedMethod({
  name: 'users.deleteUser',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    if (Meteor.isServer && Meteor.user().admin) {
      return Meteor.users.remove(_id);
    }
  },
});

export const subscribeToList = new ValidatedMethod({
  name: 'users.subscribeToList',
  validate: new SimpleSchema({
    email: { type: String },
  }).validator(),
  run({ email }) {
    if (Meteor.isServer) {
      const list = 'fa7ca8e639';
      const url = `https://us17.api.mailchimp.com/3.0/lists/${list}/members`;
      HTTP.post(url, {
        auth: `apikey:${Meteor.settings.MAILCHIMPKEY}`,
        data: {
          email_address: email,
          status: 'subscribed',
        },
      });
    }
  },
});
