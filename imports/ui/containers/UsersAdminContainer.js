import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import UsersAdminPage from '/imports/ui/pages/admin/UsersAdminPage.js';

export default withTracker(() => {
  const usersHandle = Meteor.subscribe('users.allUsers');
  const userMembership = Meteor.subscribe('users.membership');
  const loading = !usersHandle.ready();
  return {
    loading,
    users: Meteor.users.find().fetch(),
  };
})(UsersAdminPage);
