import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import App from '../layouts/App.js';

export default withTracker(() => {
  const returnObject = {
    user: Meteor.user(),
    loading,
    initialVisit: true,
  };
  const userHandle = Meteor.subscribe('users.myData');
  const userMembershipHandle = Meteor.subscribe('users.membership');
  const loading = !userHandle.ready() && !userMembershipHandle.ready();

  this.handleInitialVisit = () => {
    if (returnObject.initialVisit) {
      returnObject.initialVisit = false;
    }
  };

  returnObject.handleInitialVisit = this.handleInitialVisit;
  return returnObject;
})(App);
