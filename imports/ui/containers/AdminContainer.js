import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Admin from '/imports/ui/layouts/Admin.js';

export default withTracker(() => {
  const userHandle = Meteor.subscribe('users.myData');
  const loading = !userHandle.ready();
  return {
    user: Meteor.user(),
    loading,
  };
})(Admin);
