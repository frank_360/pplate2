import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Ratings } from '/imports/api/ratings/ratings.js';
import AllRecipesPage from '/imports/ui/pages/app/AllRecipesPage.js';

export default withTracker((props) => {
  const ratingsHandle = Meteor.subscribe('ratings.myRatings');
  const loadingS = !ratingsHandle.ready();
  return {
    user: props.user,
    ratings: Ratings.find({ userId: Meteor.userId() }).fetch(),
    loadingS,
  };
})(AllRecipesPage);
