import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Ratings } from '/imports/api/ratings/ratings.js';
import { Swaps } from '/imports/api/swaps/swaps.js';
import { getWeek } from '/imports/api/users/helpers.js';
import DashboardPage from '/imports/ui/pages/app/DashboardPage.js';

export default withTracker((props) => {
  const ratingsHandle = Meteor.subscribe('ratings.myRatings');
  const usersMembership = Meteor.subscribe('users.membership');
  const weekNumber = props.location.query.week || getWeek(props.user) || 4;
  const swapsHandle = Meteor.subscribe('swaps.myWeeklySwaps', parseInt(weekNumber, 10));
  const loadingS = !ratingsHandle.ready() || !swapsHandle.ready();

  const returnObject = {
    user: props.user,
    ratings: Ratings.find({ userId: Meteor.userId() }).fetch(),
    swaps: Swaps.find({ userId: Meteor.userId() }).fetch(),
    loadingS,
    initialVisit: true,
  };

  this.handleInitialVisit = () => {
    if (returnObject.initialVisit) {
      returnObject.initialVisit = false;
    }
  };

  returnObject.handleInitialVisit = this.handleInitialVisit;;

  return returnObject;
})(DashboardPage);
