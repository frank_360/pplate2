import React from 'react';
import AdminHeader from '/imports/ui/components/admin/AdminHeader.js';
import Loading from '/imports/ui/components/general/Loading.js';
import PropTypes from 'prop-types';

export default class Admin extends React.Component {
  render() {
    const { user, children, loading } = this.props;
    if (loading) return <div className="big-loading"><Loading /></div>;
    if (true) {
      return (
        <div className="app-container">
          <AdminHeader />
          {React.cloneElement(children, { user })}
        </div>
      );
    }
    return <div className="big-loading"><Loading /></div>;
  }
}

Admin.propTypes = {
  user: PropTypes.object,
  children: PropTypes.element,
  loading: PropTypes.bool,
};
