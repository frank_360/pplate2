import React from 'react';
import PropTypes from 'prop-types';
import MainHeader from '/imports/ui/components/general/MainHeader.js';
import LoggedOutHeader from '/imports/ui/components/general/LoggedOutHeader.js';
import Footer from '/imports/ui/components/general/Footer.js';
import Loading from '/imports/ui/components/general/Loading.js';
import { browserHistory } from 'react-router';

const DocumentTitle = require('react-document-title')

export default class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.setState({ prevPath: nextProps.location.pathname });
    }

    // if (!nextProps.loading && nextProps.user && !nextProps.user.charge_id && !pathname.includes('payment') && !pathname.includes('signup') && pathname !== '/' && !pathname.includes('coupons')) {
    //   if (!nextProps.user.profile) {
    //     window.location.href = '/signup';
    //   } else {
    //     browserHistory.push({
    //       pathname: '/payment',
    //     });
    //   }
    // }
  }
  render() {
    const { user, children, loading, location } = this.props;
    if (loading) return <div className="big-loading"><Loading /></div>;
    const payment = location.pathname.includes('payment');
    const prevPath = this.state ? this.state.prevPath : '';
    return (
      <DocumentTitle title={this.props.route.name || 'Pregnancy Plate'}>
        <div className="app-container">
          {user ? <MainHeader user={user} location={location} /> : <LoggedOutHeader /> }
          {children ? React.cloneElement(children, { user: this.props.user, initialVisit: this.props.initialVisit, handleInitialVisit: this.props.handleInitialVisit }) : ''}
          <div id="popup-mount" />
          <Footer />
        </div>
      </DocumentTitle>
    );
  }
}

App.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  children: PropTypes.element,
  loading: PropTypes.bool,
  handleInitialVisit: PropTypes.function,
};
