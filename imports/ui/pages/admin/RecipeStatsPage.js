import React from 'react';
import { Table, Tr, Td, Th, Thead } from 'reactable';
import PropTypes from 'prop-types';
import _ from 'underscore';
import { getRecipeStats } from '/imports/api/ratings/methods.js';
import Loading from '/imports/ui/components/general/Loading.js';

export default class RecipeStatsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      recipes: [],
    };
  }
  componentWillMount() {
    const self = this;
    getRecipeStats.call({}, (err, res) => {
      self.setState({ loading: false, recipes: res });
    });
  }
  render() {
    const { loading, recipes } = this.state;
    if (loading) return <div className="big-loading"><Loading /></div>;
    const Rows = _.map(recipes, (r) => {
      return (
        <Tr key={r._title}>
          <Td column="title" data={r.title}>{r.title}</Td>
          <Td column="timesMade" data={r.timesMade}>{r.timesMade}</Td>
          <Td column="avgRating" data={r.avgRating}>{r.avgRating.toFixed(2)}</Td>
        </Tr>
      );
    });
    return (
      <div className="standard-container">
        <h1>Recipes</h1>
        <Table
          className="admin-table table table-hover table-mc-light-blue"
          sortable={['title', 'timesMade', 'avgRating']}
          filterable={['title', 'timesMade', 'avgRating']}
          noDataText="No matching recipes found."
        >
          <Thead>
            <Th column="title">
              <strong>Title</strong>
            </Th>
            <Th column="timesMade">
              <strong>Times Made</strong>
            </Th>
            <Th column="avgRating">
              <strong>Average Rating</strong>
            </Th>
          </Thead>
          {Rows}
        </Table>
      </div>
    );
  }
}
