import React from 'react';
import { Table, Tr, Td, Th, Thead, Sort } from 'reactable';
import Meteor from 'meteor/meteor';
import PropTypes from 'prop-types';
import _ from 'underscore';
import { getFullName, getEmail, getWeek } from '/imports/api/users/helpers.js';
import { deleteUser } from '/imports/api/users/methods.js';
import moment from 'moment';
import swal from 'sweetalert2';
import 'sweetalert2/dist/sweetalert2.css';
import Loading from '/imports/ui/components/general/Loading.js';

export default class UsersAdminPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(e) {
    e.preventDefault();
    const id = e.currentTarget.dataset.id;
    swal({
      title: 'Are you sure?',
      text: 'You are deleting the user forever',
      type: 'warning',
    }).then((isConfirm) => {
      if (isConfirm) {
        deleteUser.call({ _id: id });
      }
    });
  }
  render() {
    const { users, loading } = this.props;
    // if (loading) return <div className="big-loading"><Loading /></div>;
    const Rows = _.map(users, (u) => {
      return (
        <Tr key={u._id}>
          <Td column="name" data={getFullName(u)}>{getFullName(u)}</Td>
          <Td column="createdAt" data={moment(u.createdAt).format('M/D/YY')}>{moment(u.createdAt).unix()}</Td>
          <Td column="email" data={getEmail(u)}>{getEmail(u)}</Td>
          <Td column="paid" data={!!u.admin}>{!!u.admin}</Td>
          <Td column="week" data={getWeek(u)}>{getWeek(u)}</Td>
          <Td column="delete">
            <a href="#" data-id={u._id} onClick={this.handleDelete}>Delete</a>
          </Td>
        </Tr>
      );
    });
    return (
      <div className="standard-container">
        <h1>Users</h1>
        <Table
          className="admin-table table table-hover table-mc-light-blue"
          sortable={[
            { column: 'createdAt', sortFunction: Sort.Date },
            'name',
            'email',
            'paid',
            'week',
          ]}
          filterable={['name', 'email', 'plan', 'week']}
          defaultSort={{ column: 'submittedAt', direction: 'desc' }}
          noDataText="No matching users found."
        >
          <Thead>
            <Th column="name">
              <strong>Name</strong>
            </Th>
            <Th column="createdAt">
              <strong>Created At</strong>
            </Th>
            <Th column="email">
              <strong>Email</strong>
            </Th>
            <Th column="paid">
              <strong>Paid</strong>
            </Th>
            <Th column="week">
              <strong>Week</strong>
            </Th>
            <Th column="delete">
              <strong>Delete</strong>
            </Th>
          </Thead>
          {Rows}
        </Table>
      </div>
    );
  }
}

UsersAdminPage.propTypes = {
  users: PropTypes.array,
  loading: PropTypes.bool,
};
