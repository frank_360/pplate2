import React from 'react';
import { Meteor } from 'meteor/meteor';
import { browserHistory, Link } from 'react-router';
import { Accounts } from 'meteor/accounts-base';

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      error: '',
      sent: false,
    };
  }
  componentWillMount() {
    if (Meteor.user()) browserHistory.push('/');
  }
  handleSubmit(e) {
    e.preventDefault();
    const email = this.email.value;
    this.setState({ error: '' });
    if (!email) {
      return this.setState({ error: 'Please enter a valid email' });
    }
    const self = this;
    Accounts.forgotPassword({ email }, (error) => {
      if (error) {
        return this.setState({ error: 'There is an error with your email' });
      }
      return self.setState({ sent: true });
    });
  }
  render() {
    const { sent, error } = this.state;
    return (
      <div className="sign-in-page">
        <div className="sign-in-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="sign-in-right-pane">
          <div className="sign-up-container">
            <span className="sign-up-error">{error}</span>
            <h3>Forgot Password</h3>
            <div className="sign-up-form-group">
              <label htmlFor="email">Email:</label>
              <input type="email" ref={(c) => { this.email = c; }} />
            </div>
            <button className="continueButton" onClick={this.handleSubmit}>{sent ? 'Sent!' : 'Send Email'}</button>
            <div className="alternative-links">
              <span className="alternative-link">
                Don't have an account? <Link to="/signup">Sign Up</Link>
              </span>
              <span className="alternative-link">
                Remember your password? <Link to="/login">Login</Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
