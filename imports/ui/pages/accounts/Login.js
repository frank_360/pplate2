import React from 'react';
import { Meteor } from 'meteor/meteor';
import { browserHistory, Link } from 'react-router';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.signInWithGoogle = this.signInWithGoogle.bind(this);
    this.signInWithFacebook = this.signInWithFacebook.bind(this);
    this.state = {
      error: '',
    };
  }
  componentWillMount() {
    if (Meteor.user()) browserHistory.push('/dashboard');
  }
  handleSubmit(e) {
    e.preventDefault();
    const email = this.email.value;
    const password = this.password.value;
    const self = this;
    if (!email || !password) {
      return this.setState({ error: 'Please enter an email and a password' });
    } else if (password.length <= 5) {
      return this.setState({ error: 'Please enter a password of at least 5 characters' });
    }
    Meteor.loginWithPassword(email, password, (err) => {
      if (err) {
        return self.setState({ error: err.message });
      }
      return window.location.href = '/dashboard';
    });
  }
  handleEnter(event) {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  }
  signInWithGoogle() {
    const self = this;
    Meteor.loginWithGoogle({}, (err) => {
      if (err) return self.setState(err.message);
      return window.location.href = '/dashboard';
    });
  }
  signInWithFacebook() {
    const self = this;
    Meteor.loginWithFacebook({}, (err) => {
      if (err) return self.setState(err.message);
      return window.location.href = '/dashboard';
    });
  }
  render() {
    return (
      <div className="sign-in-page">
        <div className="sign-in-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="sign-in-right-pane">
          <div className="sign-up-container">
            <span className="sign-up-error">{this.state.error}</span>
            <h3>Login</h3>
            <div className="social-sign-up-section">
              <button className="sign-up-social sign-up-google" onClick={this.signInWithGoogle}>Log in with Google</button>
              <button className="sign-up-social sign-up-facebook" onClick={this.signInWithFacebook}>Log in with Facebook</button>
            </div>
            <div className="social-separator"><span>Or via email</span></div>
            <div className="sign-up-form-group">
              <label htmlFor="email">Email:</label>
              <input type="email" ref={(c) => { this.email = c; }} />
            </div>
            <div className="sign-up-form-group">
              <label htmlFor="password">Password:</label>
              <input
                type="password"
                ref={(c) => { this.password = c; }}
                onKeyPress={this.handleEnter}
              />
            </div>
            <button className="continueButton" onClick={this.handleSubmit}>Login</button>
            <div className="alternative-links">
              <span className="alternative-link">
                Don't have an account? <Link to="/signup">Sign Up</Link>
              </span>
              <span className="alternative-link">
                Forgot your password? <Link to="/forgotpassword">Reset here</Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
