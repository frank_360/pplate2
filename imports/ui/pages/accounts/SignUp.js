import React, { Fragment } from 'react';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { Accounts } from 'meteor/accounts-base';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_blue.css';
import moment from 'moment';
import swal from 'sweetalert';
import { myAccount, updateProfileFromSocial } from '/imports/api/users/methods.js';
import { checkCoupon, checkPromo, createCoupon, claimGift } from '/imports/api/coupons/methods.js';
import MembershipOptions from '/imports/ui/components/accounts/MembershipOptions.js';
import ReactTooltip from 'react-tooltip';
import { sendAccountEmail } from '../../../api/coupons/methods';

function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.checkSignin = this.checkSignin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setError = this.setError.bind(this);
    this.signInWithGoogle = this.signInWithGoogle.bind(this);
    this.signInWithFacebook = this.signInWithFacebook.bind(this);
    this.state = {
      error: '',
      date: '',
      facebookOrGoogle: false,
      loading: true,
      gift: false,
      membership: null,
    };
  }
  componentWillMount() {
    this.checkSignin();
    if (this.props.location.state && this.props.location.state.membership) {
      this.setState({ membership: { ...this.props.location.state.membership } });
    }
  }
  componentWillReceiveProps(nextProps) {

    if (this.props.location.state !== nextProps.location.state) {
      this.setState({ membership: { ...nextProps.location.state.membership } });
    } else if (this.props.location.state && this.props.location.state.membership) {
      this.setState({ membership: { ...this.props.location.state.membership } });
    }
  }
  setError(error) {
    window.scrollTo(0, 0);
    this.setState({ error });
  }
  checkSignin() {
    Tracker.autorun(() => {
      if (!Meteor.user()) return this.setState({ loading: false });
      Meteor.subscribe('users.membership');
      myAccount.call({}, (err, user) => {
        const { services, socialOnboarded, charge_id } = user;
        if (services.google || services.facebook) {
          if (!socialOnboarded) return this.setState({ facebookOrGoogle: true, loading: false });
        }
        if (!charge_id) return browserHistory.push({ pathname: '/payment', state: { membership: { ...this.props.location.state.membership } } });
        return browserHistory.push('/');
      });
    });
  }
  async handleSubmit(e) {
    e.preventDefault();

    const { facebookOrGoogle, date } = this.state;
    const self = this;
    this.setError('');
    const knowDue = this.knowDue ? this.knowDue.checked : false;
    const agreeTerms = this.agreeTerms.checked;
    const couponCode = this.couponCode.value ? this.couponCode.value.toUpperCase() : '';
    let isGift = false;
    if (couponCode) {
      isGift = await checkCoupon.callPromise({ code: couponCode });
      if (!isGift) {
        const isCode = await checkPromo.call({ code: couponCode, membership: this.state.membership.title });
        if (!isCode) return this.setError('The promo code is invalid');
      }
    }
    const asgift = this.asgift ? this.asgift.checked : false;
    if (asgift) {
      const membership = this.state.membership;
      const profile = {
        couponCode,
        firstName: this.rFirstName.value,
        lastName: this.rLastName.value,
        membership: { ...this.props.location.state.membership },
      };
      const gifter = {
        firstName: this.yourFirstName.value,
        lastName: this.yourLastName.value,
        email: this.yourEmail.value,
      }
      const email = this.rEmail.value;
      if (!email) return this.setError('Please enter their email');
      else if (!validateEmail(email)) return this.setError('Their email is invalid');
      else if (!gifter.email) return this.setError('Please enter your email');
      else if (!validateEmail(gifter.email)) return this.setError('Your email is invalid');
      else if (!profile.firstName) return this.setError('Please enter their first name');
      else if (!profile.lastName) return this.setError('Please enter their last name');
      else if (!gifter.firstName) return this.setError('Please enter your first name');
      else if (!gifter.lastName) return this.setError('Please enter your last name');
      else if (!agreeTerms) return this.setError('Please agree to our terms');
      createCoupon.call({ profile, gifter, email, membership }, (err, res) => {
        browserHistory.push({ pathname: `/coupons/${res}`, state: { amount: this.props.location.state.membership } });
      });
    } else if (facebookOrGoogle) {
      const profile = {
        dueDate: date,
        couponCode,
        firstTime: !!this.firstTime.value,
        membership: this.state.membership,
      }
      if (!knowDue && !profile.dueDate) return this.setError('Please enter your due date');
      else if (!agreeTerms) return this.setError('Please agree to our terms');
      if (profile.dueDate && profile.dueDate[0]) profile.dueDate = moment(profile.dueDate[0]).toDate();
      else delete profile.dueDate;
      updateProfileFromSocial.call({ profile }, (err) => {
        if (err) return self.setError(err.message);
        return browserHistory.push('/payment');
      });
    } else {
      const email = this.email.value;
      const password = this.password.value;
      const passwordconfirm = this.passwordconfirm.value;
      const membership = this.props.location.state.membership;
      const profile = {
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        firstTime: !!this.firstTime.value,
        dueDate: date,
        couponCode,
        membership: { ...this.props.location.state.membership },
      };

      if (!email) return this.setError('Please enter your email');
      else if (!password || !passwordconfirm) return this.setError('Please enter a password');
      else if (password !== passwordconfirm) return this.setError('Your passwords do not match');
      else if (!profile.firstName) return this.setError('Please enter your first name');
      else if (!profile.lastName) return this.setError('Please enter your last name');
      else if (!knowDue && !profile.dueDate) return this.setError('Please enter your due date');
      else if (!agreeTerms) return this.setError('Please agree to our terms');

      if (profile.dueDate && profile.dueDate[0]) profile.dueDate = moment(profile.dueDate[0]).toDate();
      else delete profile.dueDate;

      Accounts.createUser({ email, password, profile, membership }, (err) => {
        if (err) {
          return self.setError(err.message);
        }
        if (isGift) {
          claimGift.call({ code: couponCode }, () => {
            sendAccountEmail.call({ email, profile }, () => {
              swal({
                title: 'Gift Claimed!',
                icon: 'success',
                button: 'Continue',
              }).then((confirm) => {
                if (confirm) return browserHistory.push({ pathname: '/payment', state: { membership: { ...this.props.location.state.membership } } });
              });
            });
          });
        } else {
          sendAccountEmail.call({ email, profile }, () => {
            console.log('email sent');
          });
          return browserHistory.push({ pathname: '/payment', state: { membership: { ...this.props.location.state.membership } } });
        }
      });
    }
  }
  signInWithGoogle(e) {
    e.preventDefault();
    const self = this;
    Meteor.loginWithGoogle({}, (err) => {
      if (err) return self.setState(err.message);
      return self.setState({ facebookOrGoogle: true });
    });
  }
  signInWithFacebook(e) {
    e.preventDefault();
    const self = this;
    Meteor.loginWithFacebook({}, (err) => {
      if (err) return self.setState(err.message);
      return self.setState({ facebookOrGoogle: true });
    });
  }
  render() {
    const { date, error, facebookOrGoogle, loading, gift } = this.state;
    const prevPathIndex = this.props.routes.length - 2;
    const prevPath = this.props.routes[prevPathIndex].path;

    if (loading) return <div />;
    const SelfSignUp = (
      <form className="sign-up-fields sign-up-self">
        <div className="sign-up-form-group">
          <label htmlFor="email">Email:</label>
          <input type="email" ref={(c) => { this.email = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="password">Password:</label>
          <input type="password" ref={(c) => { this.password = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="passwordconfirm">Re-enter Password:</label>
          <input type="password" ref={(c) => { this.passwordconfirm = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="firstName">First Name:</label>
          <input type="text" ref={(c) => { this.firstName = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="lastName">Last Name:</label>
          <input type="text" ref={(c) => { this.lastName = c; }} />
        </div>
      </form>
    );
    const GiftSignUp = (
      <div className="sign-up-gift">
        <div className="sign-up-form-group">
          <label htmlFor="email">Your Email:</label>
          <input type="email" ref={(c) => { this.yourEmail = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="firstName">First Name:</label>
          <input type="text" ref={(c) => { this.yourFirstName = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="lastName">Last Name:</label>
          <input type="text" ref={(c) => { this.yourLastName = c; }} />
        </div>
        <div className="social-separator" />
        <div className="sign-up-form-group">
          <label htmlFor="email">Recipient&#8217;s Email:</label>
          <input type="email" ref={(c) => { this.rEmail = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="firstName">Recipient&#8217;s First Name:</label>
          <input type="text" ref={(c) => { this.rFirstName = c; }} />
        </div>
        <div className="sign-up-form-group">
          <label htmlFor="lastName">Recipient&#8217;s Last Name:</label>
          <input type="text" ref={(c) => { this.rLastName = c; }} />
        </div>
      </div>
    );
    const MembershipComponent = () => {
      if (this.state.membership !== null) {
        return <MembershipOptions {...this.state.membership} prevPath={prevPath} />;
      } else if (this.props.location.state && this.props.location.state.membership) {
        return <MembershipOptions {...this.props.location.state.membership} />;
      }
      return <MembershipOptions />;
    };
    return (
      <div className="sign-in-page">
        <div className="sign-in-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="sign-in-right-pane">
          <div className="sign-up-container">
            <span className="sign-up-error">{error}</span>
            <h3>Select Your Membership</h3>
            {MembershipComponent()}
            <div className="social-separator" />
            {!facebookOrGoogle ?
              <div>
                <div className="social-sign-up-section">
                  <button className="sign-up-social sign-up-google" onClick={this.signInWithGoogle}>Sign up with Google</button>
                  <button className="sign-up-social sign-up-facebook" onClick={this.signInWithFacebook}>Sign up with Facebook</button>
                </div>
                <div className="social-separator"><span>Or via email</span></div>
                <input type="checkbox" className="as-a-gift" name="asgift" ref={(c) => { this.asgift = c; }} onChange={() => this.setState({ gift: !this.state.gift })} />I&rsquo;m giving this as a gift <span data-tip="Gift Recipients will receive an email with a special coupon code that allows her to redeem your gift and set up her own account." className="tip-q">?</span><ReactTooltip />
                {gift ? GiftSignUp : SelfSignUp}
                <div className="social-separator" />
              </div>
              : '' }

            <div className="sign-up-preg-info">
              <h5 className="sign-up-question">When is {gift ? 'their' : 'your'} due date?</h5>
              <input type="checkbox" name="knowDue" ref={(c) => { this.knowDue = c; }} /><span className="radio-text due-box">If you do not know {gift ? 'their' : 'your'} due date, check this box</span>
              <Flatpickr
                value={date}
                onChange={e => (this.setState({ date: e }))}
              />
              <h5 className="sign-up-question">Is this {gift ? 'their' : 'your'} first time pregnant?</h5>
              <div className="radio-group">
                <input type="radio" name="first-time" value={1} ref={(c) => { this.firstTime = c; }} />Yes<br />
                <input type="radio" name="first-time" value={0} ref={(c) => { this.firstTime = c; }} />No<br />
              </div>
            </div>
            <div className="social-separator" />
            <div className="end-signup">
              <input type="checkbox" name="agree-terms" ref={(c) => { this.agreeTerms = c; }} /><span className="radio-text">I agree with the <a href="/privacy" target="_blank">privacy policy</a> and have read the <a href="/disclaimers" target="_blank">disclaimers</a></span>
              <div className="sign-up-form-group coupon-group">
                <label htmlFor="lastName">Coupon Code:</label>
                <input type="text" ref={(c) => { this.couponCode = c; }} />
              </div>
            </div>
            <button className="continueButton" onClick={this.handleSubmit}>Continue</button>
          </div>
        </div>
      </div>
    );
  }
}

SignUp.propTypes = {
  location: PropTypes.object,
  routes: PropTypes.array,
};
