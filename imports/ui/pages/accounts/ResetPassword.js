import React from 'react';
import { Meteor } from 'meteor/meteor';
import { browserHistory, Link } from 'react-router';
import { Accounts } from 'meteor/accounts-base';

export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      error: '',
    };
  }
  componentWillMount() {
    if (Meteor.user()) browserHistory.push('/');
  }
  handleSubmit(e) {
    e.preventDefault();
    this.setState({ error: '' });
    const password = this.password.value;
    if (!password) return this.setState({ error: 'Please enter your password' });
    const self = this;
    const token = window.location.pathname.split('/').pop();
    return Accounts.resetPassword(token, password, (error) => {
      if (error) return self.setState({ error: 'We are unable to reset your password' });
      return browserHistory.push('/dashboard');
    });
  }
  render() {
    const { error } = this.state;
    return (
      <div className="sign-in-page">
        <div className="sign-in-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="sign-in-right-pane">
          <div className="sign-up-container">
            <span className="sign-up-error">{error}</span>
            <h3>Set Your New Password</h3>
            <div className="sign-up-form-group">
              <label htmlFor="password">Password:</label>
              <input type="password" ref={(c) => { this.password = c; }} />
            </div>
            <button className="continueButton" onClick={this.handleSubmit}>Reset Password</button>
          </div>
        </div>
      </div>
    );
  }
}
