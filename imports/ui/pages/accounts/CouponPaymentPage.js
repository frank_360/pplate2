import React from 'react';
import { getCoupon } from '/imports/api/coupons/methods.js';
import { Meteor } from 'meteor/meteor';
import { StripeProvider, Elements } from 'react-stripe-elements';
import CheckoutFormGift from '/imports/ui/components/accounts/CheckoutFormGift.js';
import scriptLoader from 'react-async-script-loader';
import { createClient } from 'contentful';

class CouponPaymentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      loadingS: true,
      coupon: false,
      amount: 12900,
      dollarsOff: 0,
    };
  }
  componentWillMount() {
    
    const self = this;
    getCoupon.call({ code: this.props.params.code }, (err, coupon) => {
      if (!err) {
        self.setState({ coupon });
        const couponCode = coupon.profile.couponCode;
        if (couponCode) {
          const client = createClient({
            space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
            accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
          });
          client.getEntries({
            content_type: 'coupons',
            'fields.code': couponCode,
          }).then((entries) => {
            if (entries.total) {
              const amount = 12900 - (entries.items[0].fields.dollarsOff * 100);
              self.setState({ amount, dollarsOff: entries.items[0].fields.dollarsOff, loading: false });
            }
          });
        } else self.setState({ loading: false });
      }
    });
  }
  componentDidMount () {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ loadingS: false });
    }
  }
  componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        this.setState({ loadingS: false });
      } else this.props.onError()
    }
  }
  render() {
    const { loading, loadingS, amount, dollarsOff } = this.state;
    if (loading || loadingS) return <div />;
    let dNumber = 129;
    if (dollarsOff) dNumber = (amount / 100).toFixed(2);
    return (
      <div className="payment-page">
        <div className="payment-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="payment-right-pane">
          <div className="payment-container">
            <h3>Make Gift Payment</h3>
            <div className="social-separator" />
            <p className="total-cost">Your Cost: {dollarsOff ? <span><span className="cross-through">$129</span> ${dNumber}</span> : '$129'}</p>
            <StripeProvider apiKey={Meteor.settings.public.STRIPE_PUBLISHABLE_KEY}>
              <Elements>
                <CheckoutFormGift />
              </Elements>
            </StripeProvider>
          </div>
        </div>
      </div>
    );
  }
}

export default scriptLoader(
  [
    'https://js.stripe.com/v3/'
  ],
)(CouponPaymentPage);
