import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { browserHistory } from "react-router";
import { isSocialAccount } from '/imports/api/users/helpers.js';
import { updateDueDate, cancelSub } from '/imports/api/users/methods.js';
import { Accounts } from 'meteor/accounts-base';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_blue.css';
import MembershipOptions from '/imports/ui/components/accounts/MembershipOptions.js';

export default class AccountPage extends React.Component {
  constructor(props) {
    super(props);
    this.savePassword = this.savePassword.bind(this);
    this.setError = this.setError.bind(this);
    this.saveDate = this.saveDate.bind(this);
    this.state = {
      error: {},
      checked: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.deleteUserSub = this.deleteUserSub.bind(this);
  }
  componentWillMount() {
    if (Meteor.user()) {
      const user = Meteor.user();
      if (!user.charge_id) browserHistory.push({ pathname: '/payment' });
    }
  }
  setError(type, message) {
    const error = {};
    error[type] = message;
    this.setState({ error });
  }
  setExpDate() {
    const user = Meteor.user();
    // const subMonths
    const expDateRaw = new Date(user.createdAt.toString());
    const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const months = ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    const expDate = `${weekdays[expDateRaw.getDay()]}. ${months[expDateRaw.getMonth()]} ${expDateRaw.getDate()}, ${expDateRaw.getFullYear()}`;

    return expDate;
  }
  handleChange() {
    this.setState({ checked: !this.state.checked });
  }
  deleteUserSub(e) {
    e.preventDefault();
    const user = Meteor.userId();
    const subId = this.props.user.charge_id;

    // if (subId === undefined)
    console.log(subId, user);
    cancelSub.call({ subId, user }, () => {
      console.log('user: ', user);
      window.localStorage.removeItem('Meteor.loginToken');
      window.localStorage.removeItem('Meteor.loginTokenExpires');
      window.localStorage.removeItem('Meteor.userId');
      browserHistory.push('/');
    });
  }
  savePassword() {
    this.setState({ error: {} });
    const password = this.password.value;
    const newpassword = this.newpassword.value;
    const newpasswordconfirm = this.newpasswordconfirm.value;
    if (!password || !newpassword || !newpasswordconfirm) return this.setError('password', 'Please fill all fields');
    else if (newpassword !== newpasswordconfirm) return this.setError('password', 'Passwords do not match');
    Accounts.changePassword(password, newpassword, (err) => {
      if (err) return this.setError('password', err.message);
      document.getElementById('passButton').textContent = 'Saved!';
    });
  }
  saveDate(date) {
    updateDueDate.call({ date: date[0] }, (err, res) => {
      if (!err) document.getElementById('saved').textContent = 'Saved!';
    });
  }
  render() {
    const { error } = this.state;
    const { user } = this.props;
    const { expDate } = user;
    const isSocial = isSocialAccount(user);
    const prevPath = this.props.location.pathname;
    const hideTrial = this.props.location.pathname.includes('/account');
    const membershipBlock = this.state.checked ? <MembershipOptions {...user.profile.membership} hideTrial={hideTrial} prevPath={prevPath} /> : null;
    let isMembership = false;
    const PasswordSection = (
      <div className="account-section standard-container">
        <div className="account-form">
          <h2>Change My Password</h2>
          <span className="error-class">{error.password}</span>
          <div className="sign-up-form-group">
            <label htmlFor="password">Password:</label>
            <input type="password" ref={(c) => { this.password = c; }} />
          </div>
          <div className="sign-up-form-group">
            <label htmlFor="newpassword">New Password:</label>
            <input type="password" ref={(c) => { this.newpassword = c; }} />
          </div>
          <div className="sign-up-form-group">
            <label htmlFor="newpasswordconfirm">Re-enter New Password:</label>
            <input type="password" ref={(c) => { this.newpasswordconfirm = c; }} />
          </div>
          <button className="continueButton" id="passButton" onClick={this.savePassword}>Save Password</button>
        </div>
      </div>
    );
    const DueDateSection = (
      <div className="account-section standard-container">
        <div className="account-form">
          <h2>Change My Due Date</h2>
          <Flatpickr
            value={user.profile.dueDate}
            onChange={this.saveDate}
          />
          <span id="saved" />
        </div>
      </div>
    );
    const CurrentMembership = (
      <div className="account-section standard-container">
        <h2>Current Membership</h2>
        <p>{user.profile.membership.title}<sub> Expires on: {expDate.slice(0, 16)}</sub></p>
        <div className="sign-up-form-group">
          <input className="membership-box" type="checkbox" checked={this.state.checked} onChange={this.handleChange} />
          <label htmlFor="changeMembership">Change my membership</label>
        </div>
        {membershipBlock}
      </div>
    );
    const CancelMembership = (
      <div className="account-section standard-container cancel-membership-btn">
        <Link onClick={this.deleteUserSub}>Cancel Membership</Link>
      </div>
    );
    if (user.charge_id) {
      isMembership = user.charge_id.includes('sub') || user.charge_id.includes('free');
    }
    return (
      <div className="account-page">
        <h1>Manage My Account</h1>
        {DueDateSection}
        {CurrentMembership}
        {isSocial ? '' : PasswordSection}
        {isMembership ? CancelMembership : null }
      </div>
    );
  }
}

AccountPage.propTypes = {
  user: PropTypes.object,
};
