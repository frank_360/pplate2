import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { StripeProvider, Elements } from 'react-stripe-elements';
import CheckoutForm from '/imports/ui/components/accounts/CheckoutForm.js';
import MembershipOptions from '/imports/ui/components/accounts/MembershipOptions.js';
import scriptLoader from 'react-async-script-loader';
import { createClient } from 'contentful';

class PaymentPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      amount: 0,
      dollarsOff: 0,
      checked: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }
  componentWillMount() {    
    const { user } = this.props;
    const isFullUser = user && user.profile ? user.profile : false;
    const coupon = isFullUser ? user.profile.couponCode : '';
    const self = this;
    let initialPrice;

    if (this.props.location.state !== undefined) {
      initialPrice = this.props.location.state.membership.price;
      this.setState({ amount: this.props.location.state.membership.price });
    } else if (this.props.user && this.props.user.profile.membership) {
      initialPrice = this.props.user.profile.membership.price;
      this.setState({ amount: this.props.user.profile.membership.price });
    }

    if (coupon) {
      const client = createClient({
        space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
        accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
      });
      client.getEntries({
        content_type: 'coupons',
        'fields.code': coupon,
      }).then((entries) => {
        if (entries.total) {
          const amount = initialPrice - (entries.items[0].fields.dollarsOff * 100);
          self.setState({ amount, dollarsOff: entries.items[0].fields.dollarsOff });
        }
      });
    }
  }
  componentDidMount() {
    const { isScriptLoaded, isScriptLoadSucceed } = this.props;
    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ loading: false });
    }
  }
  componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        this.setState({ loading: false });
      } else this.props.onError()
    }
  }
  handleChange() {
    this.setState({ checked: !this.state.checked });
  }
  render() {
    const { loading, dollarsOff } = this.state;
    if (loading) return <div />;
    let { amount } = (this.state / 100).toFixed(2);
    if (dollarsOff) amount = ((amount - dollarsOff) / 100).toFixed(2);
    let title;
    let frequency;
    let price;
    let passedPrice;

    if (this.props.location.state !== undefined) {
      ({ title, frequency, price } = this.props.location.state.membership);
      passedPrice = this.props.location.state.membership.price;
      price = (this.props.location.state.membership.price / 100).toFixed(2);
    } else if (this.props.user && this.props.user.profile.membership) {
      ({ title, frequency, price } = this.props.user.profile.membership);
      passedPrice = this.props.user.profile.membership.price;
      price = (this.props.user.profile.membership.price / 100).toFixed(2);
    }

    if (price === 0) {
      price = 'Free for 2 weeks,';
    }

    const prevPath = this.props.routes[this.props.routes.length - 2].path;
    const membershipSelected = this.props.location.membership ? <MembershipOptions {...this.props.location.state.membership} prevPath={prevPath} /> : <MembershipOptions prevPath={prevPath} />
    const membershipBlock = this.state.checked ?
    membershipSelected : null;

    return (
      <div className="payment-page">
        <div className="payment-left-pane">
          <img src="/images/homelogo.svg" className="accounts-logo" alt="" />
        </div>
        <div className="payment-right-pane">
          <div className="payment-container">
            <h3>Your Plan:</h3>
            <p className="additional-plan-details">{title}</p>
            <div className="social-separator" />
            <p className="total-cost">Your Cost:</p>
            <p className="additional-plan-details"> {dollarsOff ? <span><span className="cross-through">${price}</span> ${(this.state.amount / 100).toFixed(2)}</span> : `$${price} ${frequency}`}</p>
            <div className="change-plan-buttons">
              <input 
                type="checkbox"
                name="changePlan"
                checked={ this.state.checked } 
                onChange={ this.handleChange } />
              <label htmlFor="">Change your plan</label>
            </div>
            {membershipBlock}
            <StripeProvider apiKey={Meteor.settings.public.STRIPE_PUBLISHABLE_KEY}>
              <Elements>
                <CheckoutForm amount={passedPrice} />
              </Elements>
            </StripeProvider>
          </div>
        </div>
      </div>
    );
  }
}

PaymentPage.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.bool,
  location: PropTypes.object,
};

export default scriptLoader(
  [
    'https://js.stripe.com/v3/'
  ],
)(PaymentPage);
