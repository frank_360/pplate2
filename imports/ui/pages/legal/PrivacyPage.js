import React from 'react';

export default class PrivacyPage extends React.Component {
  render() {
    return (
      <div className="legal-page standard-container">
        <h2>PRIVACY STATEMENT</h2>
        <p>Created 12/8/17</p>
        <p>At Pregnancy Plate, we know that you value your privacy and so do we. This Privacy Policy describes the ways in which Pregnancy Plate collects, uses, discloses and transfers certain information, including your personal and non-personal information.</p>
        <p>The term &ldquo;personal information&rdquo; is used in this Privacy Policy to reference the information that can be used to uniquely identify you or contact you. The term &ldquo;non-personal information&rdquo; is used to reference the information that cannot be directly associated with you. We may collect, use, disclose, and transfer non-personal information for any purpose.</p>
        <p>We welcome your feedback, so if you have any questions about your privacy, please email us at <a href="mailto:info@pregnancyplate.com">info@pregnancyplate.com</a>.</p>
        <h2>Information We Collect</h2>
        <p>When you interact with us, we may collect both personal and non-personal information, as described below:</p>
        <p>Information that you provide to us:</p>
        <p>Examples of information you provide to us include your name, email address, physical address, zip code, due date, payment method, and telephone number. This information is collected in a number of ways, including, but not limited to, our website, mobile apps, interactions with customer service, marketing promotions, and surveys.</p>
        <p>Information that we collect automatically:</p>
        <p>There are some forms of information that we collect automatically, including:</p>
        <ul>
          <li>Information about your interactions with and use of our website and mobile app.</li>
          <li>Details about your interactions with customer support, such as the times and dates of interactions, the details of the interactions and transcripts (if applicable).</li>
          <li>Device identifiers such as the type of device you use, the version of your browser, your operating system and identifiers that can be used to uniquely identify your device.</li>
          <li>Standard web-logging information such as page views, referral URLs, ad data, IP addresses, connection information, browser versions, and operating system versions.</li>
          <li>Information collected via the use of cookies, web beacons and other technologies.</li>
          <li>Information from advertising network partners about the type of ads you have viewed or interacted with.</li>
        </ul>
        <p>Information obtained from other sources:</p>
        <p>We may supplement the information collected with information we obtain from other sources, both online and offline. This additional information may include examples such as demographic information or Internet browser behavior information. We use this data to better understand our past, current and future members and to improve the Pregnancy Plate website (&ldquo;Site&rdquo;) and/or mobile application, email list subscriptions and other features (collectively referred to as &ldquo;Services&rdquo;).</p>
        <h2>Our Use of Your Information</h2>
        <p>Pregnancy Plate uses the information that you provide in a manner that is consistent with this Privacy Policy. We use the information we collect to provide, analyze, administer, enhance and personalize our service and marketing initiatives. For example, we may use the information to:</p>
        <ul>
          <li>Identify your general geographic location, such as a city or zip code, to determine locally available products and discounts or for other local marketing efforts.</li>
          <li>Detect, prevent or investigate fraudulent uses of our Site or Services.</li>
          <li>Send you service- or marketing-related emails or push notifications.</li>
        </ul>
        <p>We may use aggregate anonymous information collected from your use of Pregnancy Plate to improve the content and functionality our Services. One such example would be to understand where our customers live in order to evaluate potential partnerships or the addition of certain meal plan offerings.</p>
        <h2>Our Disclosure of Your Information</h2>
        <p>Pregnancy Plate does not sell your information. There are, however, certain circumstances in which we may share your information with certain third parties without further notice to you, such as in the following examples:</p>
        <p>Service Providers:</p>
        <p>We use other companies and contractors (&ldquo;Service Providers&rdquo;) to perform services for us or to assist in marketing, research, advertising, communications, infrastructure, and IT services in the course of providing our Services to you.</p>
        <p>Promotional Offers: </p>
        <p>We may offer joint marketing promotions that, in order for you to participate, will require that we share your information with third parties. By participating in these joint promotions, you agree to our sharing of your information. Please note that we do not control the privacy practices of any third-party businesses or guarantee that their practices are similar to ours.</p>
        <p>Business Transfer: </p>
        <p>We may transfer your information due to any reorganization, restructuring, merger, sale, or other transfer of assets. We reserve the right to transfer our information, including your personal information, provided that the receiving party agrees to respect your personal information in a manner that is consistent with our Privacy Policy.</p>
        <p>Social Media Services: </p>
        <p>If you choose to connect and share information from our service to a social media network, the information you share is then subject to the social media service&rsquo;s privacy policy.</p>
        <p>Legal Requirements: </p>
        <p>Pregnancy Plate may disclose your information if required to do so by law or in the good faith belief that such action is necessary to comply with a legal obligation, to protect the rights or property of Pregnancy Plate, to protect the personal safety of users or to protect against legal liability.</p>
        <p>Others with access to your account: </p>
        <p>If you choose to share your account with other parties, they may have access to any of your personal information.</p>
        <h2>Cookies</h2>
        <p>Either Pregnancy Plate or our Service Providers may use cookies in your web browser or utilize tracking pixels to analyze or enhance the use of our Services. These cookies don&rsquo;t collect information from your computer, but may be used to record information that you have submitted to us. One example of our use of cookies is to enable our Site to remember who you are from visit to visit.</p>
        <p>You can erase and block these cookies using the features available on your web browser. However, should you do this, it is possible that portions of the site may not function properly or at all.</p>
        <p>Pregnancy Plate allows third-party advertisers that are presenting ads on our behalf to set and access cookies on your web browser. The advertisers&rsquo; use of cookies or tracking pixels is subject to their own privacy policy, not this Privacy Policy.</p>
        <h2>Your Choices</h2>
        <p>If you don&rsquo;t want to receive email from us, you can simply &ldquo;Unsubscribe&rdquo; by emailed <a href="mailto:info@pregnancyplate.com">info@pregnancyplate.com</a>. However, there are certain administrative email from which you can&rsquo;t unsubscribe, such as service notifications and emails related to your financial transactions with us. You may, however, opt out of informational updates and from the weekly electronic delivery of our meal plans.</p>
        <h2>Right to Access Your Information</h2>
        <p>You have access to and the ability to update a broad range of your personal information through the member area of our Site. However, if you ever have questions about the Privacy Policy or your personal information, please contact <a href="mailto:info@pregnancyplate.com">info@pregnancyplate.com</a>.</p>
        <h2>Exclusions</h2>
        <p>This Privacy Policy does not apply to any Personal Information collected by Pregnancy Plate other than the Personal Information collected through the Site or Services. It does not apply to any unsolicited information you provide to Pregnancy Plate through this Service or any other means, which includes, but is not limited to, information posted to any public areas of the Service or to related forums or social media sites. All unsolicited information is deemed non-confidential and Pregnancy Plate will be free to reproduce, use, and disclose such information without limitation or attribution.</p>
        <h2>Children</h2>
        <p>Pregnancy Plate does not knowingly collect Personal Information from children under the age of 13. If you are under the age of 13, please do not sign up for the Service or submit any Personal Information through the Service. If we learn that we have collected or received Personal Information from a child under 13 without verification of parental consent, we will delete the information.</p>
        <h2>Security</h2>
        <p>We use reasonable administrative, technical, physical and managerial measures to protect your personal information against theft, loss, and unauthorized access. Unfortunately no security system can be guaranteed to be 100% secure, and therefore, we cannot guarantee the security of your personal information and cannot assume liability for improper access to it.</p>
        <h2>Changes to This Policy</h2>
        <p>From time to time we may make changes to this Privacy Policy by posting an updated version on our website. Changes take effect immediately upon posting.</p>
        <h2>Transfer of Information</h2>
        <p>Given the global nature of our society, Pregnancy Plate conducts business globally, which may result in the transfer of your information internationally. Therefore, by using Pregnancy Plate you acknowledge and agree to the transfer of your information outside of your country of residence to any country (including the United States) where we have facilities or Service Providers. You understand that countries that we may transfer information to may not have as comprehensive a level of data protection as your country, although your personal information will continue to be protected in accordance with the standards described in this Policy.</p>
        <h2>Contacting Pregnancy Plate</h2>
        <p>For questions or comments regarding our Privacy Policy, please email <a href="mailto:info@pregnancyplate.com">info@pregnancyplate.com</a>.</p>
      </div>
    );
  }
}
