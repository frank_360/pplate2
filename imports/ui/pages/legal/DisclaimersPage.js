import React from 'react';

export default class DisclaimersPage extends React.Component {
  render() {
    return (
      <div className="legal-page standard-container">
        <h2>Medical Disclaimer</h2>
        <p>
          Created 12/8/17
        </p>
        <p>
          The medical and/or nutritional information on pregnancyplate.com (the “Website”), is provided for general educational and informational purposes only.  The Website does not provide medical or legal advice.  Neither viewing the Website, receiving information contained on the Website, nor the transmission of information to or from the Website, creates a physician-patient or attorney-client relationship between you and the Website or Pregnancy Plate, LLC.  The information on the Website is not intended as, nor should it be considered a substitute for, professional medical advice, suggestions, diagnosis, counseling, or treatment of any kind.
        </p>
        <p>
          While the recipes on pregnancyplate.com are reviewed and validated by a registered dietitian, the Website cannot and does not provide medical or dietary advice specific to any individual’s situation.  The statements made on the Website have not been approved by the Food and Drug Administration, and are not intended to diagnose, treat, cure, or prevent any disease.
        </p>
        <p>
          Always follow safe food handling guidelines when preparing food.
        </p>
        <p>
          The information on the Website is provided without any guarantees.  Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition.  Never disregard professional medical advice or delay seeking it because of something you have read on the Website.  Full medical clearance from a licensed physician should be obtained before beginning or modifying any diet, exercise, or lifestyle program, and your physician should be informed of all nutritional changes.  The information on the Website is to be used at your own risk based on your own judgment.  You assume full responsibility and liability for your own actions.
        </p>
        <h2>Affiliate Disclaimer</h2>
        <p>We earn a small commission for my endorsement, recommendation, testimonial, and/or link to any products or services from this website.</p>
      </div>
    );
  }
}
