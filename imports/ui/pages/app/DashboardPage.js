import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import NutrientOfTheWeek from '/imports/ui/components/dashboard/NutrientOfTheWeek.js';
import DashWelcome from '/imports/ui/components/dashboard/DashWelcome.js';
import WeekButtons from '/imports/ui/components/dashboard/WeekButtons.js';
import WeeklyDinners from '/imports/ui/components/dashboard/WeeklyDinners.js';
import RecommendedSwaps from '/imports/ui/components/dashboard/RecommendedSwaps.js';
import WeeklyTips from '/imports/ui/components/dashboard/WeeklyTips.js';
import Loading from '/imports/ui/components/general/Loading.js';
import { getWeek } from '/imports/api/users/helpers.js';
import { createClient } from 'contentful';
import { browserHistory } from 'react-router';
import _ from 'underscore';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

export default class DashboardPage extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.withRating = this.withRating.bind(this);
    this.checkForSwaps = this.checkForSwaps.bind(this);
    this.swapped = this.swapped.bind(this);
    this.state = {
      loading: true,
      week: {},
      weekNumber: 4,
      recipes: {},
      tips: {},
    };
  }
  componentWillMount() {
    const user = Meteor.user();

    if (user === undefined) {
      browserHistory.push({ pathname: '/' });
    } else if (user && user.charge_id === undefined) {
      browserHistory.push({ pathname: '/payment' });
    } else if (!Meteor.userId()) {
      browserHistory.push({ pathname: '/' });
    }

  }
  componentWillReceiveProps(nextProps) {
    const { location, loadingS, swaps, user } = nextProps;
    const { weekNumber } = this.state;
    if (location.query.week && location.query.week !== weekNumber.toString()) {
      this.setState({ loading: true });
      this.getData(nextProps);
    } else if (this.props.loadingS !== loadingS) {
      this.setState({ loading: true });
      this.getData(nextProps);
    }
  }
  componentWillUnmount() {
    this.props.handleInitialVisit();
  }
  getData(props, num = 1) {
    const { location, user } = props;
    const weekNumber = location.query.week || getWeek(user) || 4;
    this.setState({ weekNumber: parseInt(weekNumber, 10) });
    const self = this;
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    client.getEntries({
      content_type: 'week',
      'fields.number': weekNumber,
      include: 1,
    }).then((entries) => {
      if (entries.items && entries.items[0]) {
        const week = entries.items[0].fields;
        self.setState({
          week,
          recipes: {
            sunday: week.sunday ? this.withRating(week.sunday) : {},
            monday: week.monday ? this.withRating(week.monday) : {},
            tuesday: week.tuesday ? this.withRating(week.tuesday) : {},
            wednesday: week.wednesday ? this.withRating(week.wednesday) : {},
            thursday: week.thursday ? this.withRating(week.thursday) : {},
          },
          tips: {
            weeklyTip: week.weeklyTip,
            weeklyTipImage: week.weeklyTipImage ? week.weeklyTipImage.fields.file.url : '',
            breakfastTip: week.breakfastTip,
            breakfastTipImage: week.breakfastTipImage ? week.breakfastTipImage.fields.file.url : '',
            lunchTip: week.lunchTip,
            lunchTipImage: week.lunchTipImage ? week.lunchTipImage.fields.file.url : '',
            snackTip: week.snackTip,
            snackTipImage: week.snackTipImage ? week.snackTipImage.fields.file.url : '',
          },
        });
        self.checkForSwaps(props, num);
      } else {
        self.setState({
          week: {},
          recipes: {},
          tips: {},
        });
        self.setState({ loading: false });
      }
    });
  }
  checkForSwaps(props, num) {
    const { recipes } = this.state;
    const { swaps } = props;
    const self = this;
    const list = [];
    _.each(recipes, (r, key) => {
      const swap = _.findWhere(swaps, { day: key });
      if (swap) list.push({ recipeId: swap.recipeId, day: key });
    });
    if (list.length === 0) return this.setState({ loading: false, num });
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    client.getEntries({
      content_type: 'recipe',
      'sys.id[in]': _.pluck(list, 'recipeId').toString(),
    }).then((entries) => {
      if (entries.items) {
        _.each(swaps, (e) => {
          const recipe = _.find(entries.items, item => item.sys.id === e.recipeId);
          if (recipe) {
            const day = e.day;
            recipe.fields.swap = true;
            recipes[day] = self.withRating(recipe);
          }
        });
      }
      self.setState({ loading: false, recipes, num });
    });
  }
  withRating(recipe) {
    const { ratings, user } = this.props;
    const recipeId = recipe.sys.id;
    const rating = _.findWhere(ratings, { recipeId });
    const stars = rating ? rating.stars : 0;
    return _.extend(recipe.fields, {
      stars,
      id: recipeId,
      madeThis: _.contains(user.madeRecipes || [], recipeId),
    });
  }
  swapped() {
    this.getData(this.props, Math.random());
  }
  render() {
    const { user, loadingS } = this.props;
    const { loading, week, weekNumber, tips, recipes, num } = this.state;
    if (loadingS) return <div className="big-loading"><Loading /></div>;
    if (loading) return <div className="big-loading"><Loading /></div>;
    const bColor = {
      backgroundImage: week.backgroundImage ? `url(${week.backgroundImage.fields.file.url})` : '',
      backgroundColor: week.color ? shadeColor(week.color, 0.70) : '',
    };
    return (
      <div className="dashboard-page">
        <div className="dashboard-main">
          <div className="dashboard-splash" style={bColor}>
            <WeekButtons week={week} weekNumber={weekNumber} />
            <DashWelcome week={week} weekNumber={weekNumber} user={user} />
          </div>
          <NutrientOfTheWeek week={week} />
        </div>
        <WeeklyDinners recipes={recipes} weekNumber={weekNumber} color={week.color} swapped={this.swapped} num={num} />
        <div className="swaps-and-tips">
          <RecommendedSwaps weekNumber={weekNumber} color={week.color} swapped={this.swapped} />
          <WeeklyTips color={week.color} tips={tips} />
        </div>
      </div>
    );
  }
}

DashboardPage.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  ratings: PropTypes.array,
  swaps: PropTypes.array,
  loadingS: PropTypes.bool,
  handleInitialVisit: PropTypes.function,
};

DashboardPage.defaultProps = {
  ratings: [],
  swaps: [],
};
