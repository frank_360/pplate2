import React from "react";
import PropTypes from "prop-types";
import { Meteor } from "meteor/meteor";
import { browserHistory } from "react-router";
import { createClient } from "contentful";
import _ from "underscore";
import AllRecipeBlock from "/imports/ui/components/recipes/AllRecipeBlock.js";
import RecipeShow from "/imports/ui/components/recipes/RecipeShow.js";
import Loading from "/imports/ui/components/general/Loading.js";
import { getWeek } from "/imports/api/users/helpers.js";

export default class AllRecipesPage extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.withRating = this.withRating.bind(this);
    this.showRecipeSwitch = this.showRecipeSwitch.bind(this);
    this.state = {
      loading: true,
      recipes: {},
      tips: {},
      showRecipe: false
    };
  }
  componentWillMount() {
    if (!Meteor.userId()) browserHistory.push({ pathname: "/" });
    this.getData(this.props);
  }
  getData(props) {
    const { location, user } = props;
    const weekNumber = location.query.week || getWeek(user) || 4;
    this.setState({ weekNumber: parseInt(weekNumber, 10) });
    const self = this;
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN
    });
    client
      .getEntries({
        content_type: "recipe",
        include: 1
      })
      .then(entries => {
        if (entries.items) {
          const recipes = _.map(entries.items, e => self.withRating(e));
          self.setState({ recipes, loading: false });
        }
      });
  }
  withRating(recipe) {
    const { ratings, user } = this.props;
    const recipeId = recipe.sys.id;
    const rating = _.findWhere(ratings, { recipeId });
    const stars = rating ? rating.stars : 0;
    return _.extend(recipe.fields, {
      stars,
      id: recipeId,
      madeThis: _.contains(user.madeRecipes || [], recipeId)
    });
  }
  showRecipeSwitch(recipeId) {
    const { recipes } = this.state;
    if (!recipeId) this.setState({ showRecipe: false });
    this.setState({ showRecipe: _.findWhere(recipes, { id: recipeId }) });
  }
  render() {
    const { loadingS } = this.props;
    const { loading } = this.state;
    if (loadingS || loading)
      return (
        <div className="big-loading">
          <Loading />
        </div>
      );
    const { recipes, showRecipe } = this.state;
    if (showRecipe) {
      return (
        <div className="standard-page all-recipes">
          <div
            className="browse-all-bar back-bar"
            onClick={() => this.setState({ showRecipe: false })}
          >
            Back
          </div>
          <RecipeShow recipe={showRecipe} show={this.showRecipeSwitch} />;
        </div>
      );
    }
    const rated = _.sortBy(_.filter(recipes, r => r.stars), "stars").reverse();
    const other = _.sortBy(_.where(recipes, { stars: 0 }), "title");
    const AllRated = _.map(rated, r => (
      <AllRecipeBlock recipe={r} key={r.id} show={this.showRecipeSwitch} />
    ));
    const AllOther = _.map(other, r => (
      <AllRecipeBlock recipe={r} key={r.id} show={this.showRecipeSwitch} />
    ));
    return (
      <div className="standard-page all-recipes">
        <div className="browse-all-bar" />
        <div className="recipe-groups">
          <div className="recipe-group">
            <span className="recipe-group-title">My Rated Recipes</span>
            {rated ? AllRated : ""}
          </div>
          <div className="recipe-group">
            <span className="recipe-group-title">More Recipes</span>
            {AllOther}
          </div>
        </div>
      </div>
    );
  }
}

AllRecipesPage.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object,
  ratings: PropTypes.array,
  loadingS: PropTypes.bool
};

AllRecipesPage.defaultProps = {
  ratings: []
};
