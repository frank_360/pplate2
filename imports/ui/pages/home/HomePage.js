import React from "react";
import { Meteor } from "meteor/meteor";
import { Tracker } from 'meteor/tracker';
import { Link, browserHistory } from "react-router";
import { createClient } from "contentful";
import { PropTypes } from 'prop-types';

import MembershipOptions from '/imports/ui/components/accounts/MembershipOptions.js';

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      content: {},
      membership: {},
      arr: [
        { name: 'pfi1', isActive: false, label: 'Reduce Symptoms' },
        { name: 'pfi2', isActive: false, label: 'Lower Risk' },
        { name: 'pfi3', isActive: false, label: 'Healthy Weight'},
        { name: 'pfi4', isActive: false, label: 'Healthy for Life'},
        { name: 'pfi5', isActive: false, label: 'Taste Buds' },
        { name: 'pfi6', isActive: false, label: 'Faster Recovery' },
      ],
      subtn_arr: [
        {label: "HeaderSignup"},
        {label: "Nourish"},
        {label: "GetStarted1"},
        {label: "GetStarted2"},
        {label: "GetStarted3"},

      ],
    };

    this.onClick = this.onClick.bind(this);

  }
  componentWillMount() {
    // this.setScroll();
    // if (Meteor.userId() && this.props.initialVisit === true) {
    //   browserHistory.push({ pathname: '/dashboard', state: { initialVisit: false } });
    // }

    const self = this;
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN
    });
    client
      .getEntries({
        content_type: "home",
        "fields.number": 1,
        include: 1
      })
      .then((entries) => {
        if (entries.items && entries.items[0]) {
          self.setState({ content: entries.items[0].fields, loading: false });
        }
      });
  }
  onClick(index) {
    let tmp = this.state.arr;
    tmp[index].isActive = !tmp[index].isActive;
    this.setState({ arr: tmp });
    analytics.track("ButtonPressed",{category : "MoreInfo", label: tmp[index].label});
  }
  suButton(index) {
    let tmp = this.state.subtn_arr;
    analytics.track("ButtonPressed",{ category : "Signup", label: tmp[index].label });
  }

  render() {
    const { loading, content } = this.state;
    if (loading) return <div />;

    const prevPathIndex = this.props.routes.length - 1;
    const prevPath = this.props.routes[prevPathIndex].path;

    return (
      <div className="home-page">
        <div className="splash-header">
          <h2 className="sheader-title">{content.greeting}</h2>
          <p className="sheader-subtitle">{content.subGreeting}</p>
          <img src={content.mainLogo.fields.file.url} alt="" className="home-image"/>
          <p className="sheader-subtitle">{content.subGreeting2}</p>
          {/*}<Link to="/signup" className="sign-up-bar">
            <span>Sign Up!</span>
          </Link>
          */}
        </div>
        <div className="purple-section">
          <div className="purple-features-section">
            <h3 className="purple-feature-header">{content.purpleHeader}</h3>
            <div className="purple-feature-items">
            {this.state.arr.map((pfi,index)  =>
             <div key={index} onClick={() => this.onClick(index)} className={"purple-feature-item collapsible" + (pfi.isActive ? 'active' : '')}>
              <div className="pfi_tr">
                <div className="purple-feature-item-header">
                  {content.purpleFeatures[index].fields.title}
                </div>
                <div className={"purple-feature-item-header-plus" + (pfi.isActive ? ' activeheader' : '')}>
                </div>
              </div>
              <div className={"purple-feature-item-desc" + " collapse-content" + (pfi.isActive ? ' show' : '')} >
                {content.purpleFeatures[index].fields.description}
              </div>
            </div>
            )}
            </div>
          </div>
          <div className="purple-bottom-section">
            <h3 className="purple-bottom-footer">{content.purpleFooter}</h3>
            <div className="sign-up-bar-mid">
              <Link to="/signup">
                <button className="sign-up-button" onClick={() => this.suButton(1)}>Nourish Your Baby</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="blue-section">
          <div className="blue-features-section">
            <h3 className="features-header">{content.blueHeader}</h3>
            <div className="features-sep" />
            <div className="blue-feature-group">
              <div className="blue-feature-item ba">
                <img src={content.blueFeatures[0].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[0].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[0].fields.description}
                </span>
              </div>
              <div className="blue-feature-item bb">
                <img src={content.blueFeatures[1].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[1].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[1].fields.description}
                </span>
              </div>
              <div className="blue-feature-item bc">
                <img src={content.blueFeatures[2].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[2].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[2].fields.description}
                </span>
              </div>
              <div className="blue-feature-item bd">
                <img src={content.blueFeatures[3].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[3].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[3].fields.description}
                </span>
              </div>
              <div className="blue-feature-item be">
                <img src={content.blueFeatures[4].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[4].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[4].fields.description}
                </span>
              </div>
              <div className="blue-feature-item bf">
                <img src={content.blueFeatures[5].fields.file.url} alt="" />
                <span className="feature-item-header">
                  {content.blueFeatures[5].fields.title}
                </span>
                <span className="feature-item-desc">
                  {content.blueFeatures[5].fields.description}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="features-sep" />
        <div className="red-section">
          <div className="red-top-section">
            <h2 className="big-red">{content.redHeader}</h2>
            <div className="red-feature-items">
              <div className="red-bullet r1">
                <img src={content.bullets[0].fields.file.url} alt="" />
              </div>
              <div className="red-feature-item r2">
                {content.redFeatures[0].fields.description}
              </div>
              <div className="red-bullet r3">
                <img src={content.bullets[0].fields.file.url} alt="" />
              </div>
              <div className="red-feature-item r4">
                {content.redFeatures[1].fields.description}
              </div>
              <div className="red-bullet r5">
                <img src={content.bullets[0].fields.file.url} alt="" />
              </div>
              <div className="red-feature-item r6">
                {content.redFeatures[2].fields.description}
              </div>
            </div>
          </div>
          <div className="red-bottom-section">
            <span className="rbs-small">Here&#8127;s How it Works</span>
            <div className="how-it-works-options">
              <div className="hiw-option ha">
                <span className="hiw-number">1</span>
                <img src={content.howItWorksSteps[0].fields.file.url} alt="" />
                <span className="hiw-title">
                  {content.howItWorksSteps[0].fields.title}
                </span>
                <span className="hiw-desc">
                  {content.howItWorksSteps[0].fields.description}
                </span>
              </div>
              <div className="hiw-option hb">
                <span className="hiw-number">2</span>
                <img src={content.howItWorksSteps[1].fields.file.url} alt="" />
                <span className="hiw-title">
                  {content.howItWorksSteps[1].fields.title}
                </span>
                <span className="hiw-desc">
                  {content.howItWorksSteps[1].fields.description}
                </span>
              </div>
              <div className="hiw-option hc">
                <span className="hiw-number">3</span>
                <img src={content.howItWorksSteps[2].fields.file.url} alt="" />
                <span className="hiw-title">
                  {content.howItWorksSteps[2].fields.title}
                </span>
                <span className="hiw-desc">
                  {content.howItWorksSteps[2].fields.description}
                </span>
              </div>
            </div>
            <div className="sign-up-bar-mid">
              <Link to="/signup">
                <button className="sign-up-button" onClick={() => this.suButton(2)}>Get Started!</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="orange-section">
          <h2>A Pregnancy Plate membership includes:</h2>
          <div className="included-features">
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[0].fields.title}
              </div>
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[1].fields.title}
              </div>
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[2].fields.title}
              </div>
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[3].fields.title}
              </div>
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[4].fields.title}
              </div>
              <div className="if-bullet">
                <img src={content.bullets[1].fields.file.url} alt="" />
              </div>
              <div className="included-feature">
                {content.includedFeatures[5].fields.title}
              </div>
          </div>
          <div className="sign-up-bar-mid">
            <Link to="/signup">
              <button className="sign-up-button" onClick={() => this.suButton(3)}>Yes! Get Started!</button>
            </Link>
          </div>
        </div>
        <div className="sign-up-section">
          <h2>Choose one of our flexible membership options or a 2-week trial</h2>
          <div className="features-sep" />
          <MembershipOptions prevPath={prevPath} />
          <div className="features-sep" />
        </div>
        <div className="green-section">
          <div className="green-top">
            <div className="green-left">
              <h3 className="b-green-left">We Can Help!</h3>
              <span className="s-green-left">
                {content.customizeMealsDescription}
              </span>
            </div>
            <div className="green-right">
              <div className="meal-selection-group">
                <div className="meal-selection">
                  <span className="meal-select-text">
                    {content.customMealExamples[0].fields.description}
                  </span>
                  <div
                    className="meal-select-image"
                    style={{
                      backgroundImage: `url('${
                        content.customMealExamples[0].fields.file.url
                      }')`
                    }}
                  />
                </div>
                <div className="meal-selection">
                  <span className="meal-select-text">
                    {content.customMealExamples[1].fields.description}
                  </span>
                  <div
                    className="meal-select-image"
                    style={{
                      backgroundImage: `url('${
                        content.customMealExamples[1].fields.file.url
                      }')`
                    }}
                  />
                </div>
                <div className="meal-selection">
                  <span className="meal-select-text">
                    {content.customMealExamples[2].fields.description}
                  </span>
                  <div
                    className="meal-select-image"
                    style={{
                      backgroundImage: `url('${
                        content.customMealExamples[2].fields.file.url
                      }')`
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="green-bottom">
            <span>{content.bottomText}</span>
          </div>
          <div className="sign-up-bar-mid">
            <Link to="/signup">
              <button className="sign-up-button" onClick={() => this.suButton(4)}>Yes! Get Started!</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
