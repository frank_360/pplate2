import React from 'react';
import { Link } from 'react-router';

export default class AdminHeader extends React.Component {
  render() {
    return (
      <div className="admin-header">
        <Link to="/manager">Users</Link>
        <Link to="/manager/recipes">Recipes</Link>
      </div>
    );
  }
}
