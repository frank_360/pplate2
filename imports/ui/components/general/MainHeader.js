import React, { Fragment } from "react";
import { render } from "react-dom";
import { browserHistory } from "react-router";
import { Meteor } from "meteor/meteor";
import PropTypes from "prop-types";
import { Link } from "react-router";
import { getFullName, getWeek } from "/imports/api/users/helpers.js";
import ShoppingList from "/imports/ui/components/recipes/ShoppingList.js";
import { slide as Menu } from "react-burger-menu";

export default class MainHeader extends React.Component {
  constructor(props) {
    super(props);
    this.openShoppingList = this.openShoppingList.bind(this);
    this.logout = this.logout.bind(this);
    this.state = {
      mobileOpen: false
    };
  }
  openShoppingList(e) {
    e.preventDefault();
    this.setState({ mobileOpen: false });
    const { location, user } = this.props;
    const weekNumber = location.query.week || getWeek(user) || 4;
    document.getElementById("popup-mount").style.display = "flex";
    render(
      <ShoppingList weekNumber={parseInt(weekNumber, 10)} />,
      document.getElementById("popup-mount")
    );
  }
  logout(e) {
    if (e) e.preventDefault();

    Meteor.logout(() => {
      browserHistory.push("/");
    });
  }
  render() {
    const { user } = this.props;
    const { mobileOpen } = this.state;
    let username = getFullName(user);

    if (user.profile === undefined) {
      this.logout();
    }
    return (
      <div className="main-header">
        <div className="standard-container">
          <div className="left-header-options">
            {Meteor.user().charge_id || Meteor.user().status === 'active' ? (
              <Fragment>
                {Meteor.user().charge_id ? <Link to="/dashboard">Dashboard</Link> : ''}
                {Meteor.user().charge_id ? <Link to="#" onClick={this.openShoppingList}>
                  Shopping List
                </Link> : ''}
                {/* <Link to="/browseall">Recipes</Link> */} 
                
                <a href="https://blog.pregnancyplate.com/tools-for-efficient-cooks/">Kitchen Tools</a>
                <a href="https://blog.pregnancyplate.com/about/">Team</a>
                <a href="https://blog.pregnancyplate.com/faq/">FAQ</a>
              </Fragment>
            ) : (
              ""
            )}
          </div>
          <div className="header-logo">
            <Link to={user && user.charge_id ? "/dashboard" : "/"}>Pregnancy Plate</Link>
          </div>
          <div className="right-header-options">
            <Link to="/account">{username}</Link>
            <div className="menu-separator" />
            <Link to="#" onClick={this.logout}>
              Sign Out
            </Link>
          </div>
        </div>
        <div className="mobile-menu">
          <Menu
            isOpen={mobileOpen}
            onClick={() => this.setState({ mobileOpen: !mobileOpen })}
          >
            <Link to="/dashboard">Dashboard</Link>
            <Link onClick={this.openShoppingList}>Shopping List</Link>
            <Link to="/account">Account</Link>
            <a href="https://blog.pregnancyplate.com/tools-for-efficient-cooks/">Kitchen Tools</a>
            <a href="https://blog.pregnancyplate.com/about/">Team</a>
            <a href="https://blog.pregnancyplate.com/faq/">FAQ</a>
            <Link to="#" onClick={this.logout}>
              Sign Out
            </Link>
          </Menu>
          <Link to="/dashboard" className="company-logo">
            Pregnancy Plate
          </Link>
        </div>
      </div>
    );
  }
}

MainHeader.propTypes = {
  user: PropTypes.object,
  location: PropTypes.object
};
