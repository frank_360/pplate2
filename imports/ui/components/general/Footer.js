import React from 'react';
import { Link } from 'react-router';

export default class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div>
          <span>&copy; {(new Date()).getFullYear()} Pregnancy Plate, LLC. All Rights Reserved.</span>
        </div>
        <div className="footer-links">
          <a href="mailto:info@pregnancyplate.com" target="_blank">info@pregnancyplate.com</a>
          <div></div>
          <a target="_blank" href="/privacy">Privacy</a>
          <a target="_blank" href="/terms">Terms</a>
          <a target="_blank" href="/disclaimers">Disclaimers</a>
        </div>
      </div>
    );
  }
}
