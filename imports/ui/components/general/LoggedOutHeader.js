import React from 'react';
import { Link } from 'react-router';
import { slide as Menu } from 'react-burger-menu';
import swal from 'sweetalert';
import { subscribeToList } from '/imports/api/users/methods.js';

export default class LoggedOutHeader extends React.Component {
  constructor(props) {
    super(props);
    this.freeMonth = this.freeMonth.bind(this);
    this.state = {
      mobileOpen: false,
    };
  }
  freeMonth() {
    // e.preventDefault();
    // setTimeout(() => {
    //   swal({
    //     title: 'Subscribe to our newsletter!',
    //     text: 'Enter your email below:',
    //     button: 'Send it to me!',
    //     content: 'input',
    //   }).then((value) => {
    //     subscribeToList.call({ email: value });
    //     swal("Awesome!", "You'll receive the next issue of our newsletter.", "success");
    //   });
    // }, 3000);
  }
  render() {
    const { mobileOpen } = this.state;
    this.freeMonth();
    return (
      <div className="main-header">
        <div className="standard-container">
          <div className="left-header-options">
            <a href="https://blog.pregnancyplate.com/tools-for-efficient-cooks/">Kitchen Tools</a>
            <a href="https://blog.pregnancyplate.com/about/">Team</a>
            {/* <a href="https://blog.pregnancyplate.com/pricing/">Pricing</a> */}
            <a href="https://blog.pregnancyplate.com/faq/">FAQ</a>
          </div>
          <div className="header-logo">
            <Link to="/">Pregnancy Plate</Link>
          </div>
          <div className="right-header-options right-logged-out">
            <a href="/signup">FREE 2 WEEK TRIAL</a>
            <Link to="/login">Login</Link>
            <Link to="/signup">Sign Up</Link>
          </div>
        </div>
        <div className="mobile-menu">
          <Menu isOpen={mobileOpen} onClick={() => this.setState({ mobileOpen: !mobileOpen })}>
            <a href="/signup">FREE 2 WEEK TRIAL</a>
            <a href="https://blog.pregnancyplate.com/tools-for-efficient-cooks/">Kitchen Tools</a>
            <a href="https://blog.pregnancyplate.com/about/">Team</a>
            <a href="https://blog.pregnancyplate.com/faq/">FAQ</a>
            <Link to="/login">Login</Link>
            <Link to="/signup">Sign Up</Link>
          </Menu>
          <Link to="/" className="company-logo">
            Pregnancy Plate
          </Link>
        </div>
      </div>
    );
  }
}
