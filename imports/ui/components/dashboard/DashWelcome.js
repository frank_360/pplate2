import React from 'react';
import PropTypes from 'prop-types';
import { getFirstName } from '/imports/api/users/helpers.js';

export default class DashWelcome extends React.Component {
  render() {
    const { week, weekNumber, user } = this.props;
    const color = {
      color: week.color || '',
    };
    return (
      <div className="week-greeting">
        <h1 style={color}>Hello, {getFirstName(user)}!</h1>
        <h3 style={color}>This is your dashboard for week {weekNumber}.</h3>
      </div>
    )
  }
}

DashWelcome.propTypes = {
  weekNumber: PropTypes.number,
  week: PropTypes.object,
  user: PropTypes.object,
};
