import React from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import marked from 'marked';
import { newRating, madeThisToggle, isMade } from '/imports/api/ratings/methods.js';
import { getNote, updateNotes } from '/imports/api/notes/methods.js';
import { X } from 'react-feather';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

function printCanvas(canvas)  {
  let dataUrl = canvas.toDataURL(); //attempt to save base64 string to server using this let
  let windowContent = '<!DOCTYPE html>';
  windowContent += '<html>'
  windowContent += '<head><title>Print canvas</title></head>';
  windowContent += '<body>'
  windowContent += '<img src="' + dataUrl + '">';
  windowContent += '</body>';
  windowContent += '</html>';
  const printWin = window.open('','','width=340,height=260');
  printWin.document.open();
  printWin.document.write(windowContent);
  printWin.document.close();
  printWin.focus();
  printWin.print();
  printWin.close();
}

export default class RecipeFull extends React.Component {
  constructor(props) {
    super(props);
    this.giveRating = this.giveRating.bind(this);
    this.madeThis = this.madeThis.bind(this);
    this.handleNotes = this.handleNotes.bind(this);
    this.print = this.print.bind(this);
    this.state = {
      notes: false,
      note: '',
    };
  }
  componentWillMount() {
    const { recipe } = this.props;
    const self = this;
    getNote.call({ recipeId: recipe.id }, (err, res) => {
      if (res) self.setState({ note: res.text });
    });
    isMade.call({ recipeId: recipe.id }, (err, res) => {
      self.setState({ madeThis: res });
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.recipe.id !== this.props.recipe.id) {
      this.setState({ note: '' });
      const self = this;
      getNote.call({ recipeId: nextProps.recipe.id }, (err, res) => {
        if (res) self.setState({ note: res.text });
      });
    }
  }
  giveRating(e) {
    const { recipe, changeRecipe, day } = this.props;
    const stars = parseInt(e.currentTarget.dataset.star, 10);
    newRating.call({ recipeId: recipe.id, stars }, (err) => {
      if (!err) {
        recipe.stars = stars;
        changeRecipe(day, recipe);
      }
    });
  }
  madeThis() {
    const { recipe } = this.props;
    const { madeThis } = this.state;
    const self = this;
    madeThisToggle.call({ recipeId: recipe.id, made: madeThis }, (err) => {
      if (!err) {
        self.setState({ madeThis: !madeThis });
      }
    });
  }
  handleNotes(e) {
    const { recipe } = this.props;
    const value = e.currentTarget.value;
    this.setState({ note: value });
    updateNotes.call({ recipeId: recipe.id, text: value });
  }
  print() {
    window.print();
  }
  render() {
    const { color, recipe, day, openRecipe } = this.props;
    const { notes, note, madeThis } = this.state;
    const { stars } = recipe;
    const bColor = {
      backgroundColor: color || '',
    };
    const lColor = {
      backgroundColor: color ? shadeColor(color, 0.40) : '',
    };
    const tColor = {
      color: color || '',
    };
    const bImage = {
      backgroundColor: color || '',
      backgroundImage: recipe.image ? `url(${recipe.image.fields.file.url})` : '',
    };
    const button = {
      borderColor: color,
      color: madeThis ? 'white' : color,
      backgroundColor: madeThis ? color : 'white',
    };
    const notesStyle = {
      zIndex: notes ? 294 : '',
    };
    const textareaStyle = {
      caretColor: color,
      borderColor: color,
    };
    const iconStyle = {
      stroke: color,
      borderColor: color,
    };
    const ingredients = _.map(recipe.ingredients, (i) => {
      if (!i) return '';
      if (i.charAt(0) === '@') return <h6 className="ingredients-separator" key={i}>{i.substring(1)}</h6>;
      const iMap = i.split(/@(.+)/);
      if (!iMap[1]) {
        iMap[1] = iMap[0];
        iMap[0] = '';
      }
      return (
        <div className="ingredients-group" key={i}>
          <span>{iMap[0].trim()}</span>
          <span>{iMap[1].trim()}</span>
        </div>
      );
    });
    const Directions = recipe.directions ? marked(recipe.directions).replace(/<a\s+href=/gi, '<a target="_blank" href=') : '';
    const RecipeBody = (
      <div className="recipe-body" id="parea">
        <div className="recipe-ingredients" style={tColor}>
          <h4 className="recipe-body-title">Ingredients</h4>
          <div className="ingredients-list">
            {ingredients}
          </div>
        </div>
        <div className="directions-list" style={tColor}>
          <h4 className="recipe-body-title">Directions</h4>
          <div className="directions-content" target="_blank" dangerouslySetInnerHTML={{ __html: Directions }} />
        </div>
      </div>
    );
    const NotesHolder = (
      <div className="notes">
        <h4 className="recipe-body-title" style={tColor}>Recipe Notes:</h4>
        <textarea className="notes-box" value={note} style={textareaStyle} onChange={this.handleNotes} />
      </div>
    );
    return (
      <div className="full-recipe">
        <div className="full-recipe-menu">
          <div className="calendar-top" style={lColor} onClick={() => this.setState({ notes: false })}>
            <div className="calendar-hole" style={bColor} />
            <div className="calendar-hole calendar-hole-right" style={bColor} />
            <span className="calendar-day" style={tColor}>{day}</span>
          </div>
          <div className="notes-menu-tab" style={notesStyle} onClick={() => this.setState({ notes: true })}>
            Notes
          </div>
        </div>
        <div className="recipe-holder">
          <div className="recipe-header">
            <div className="recipe-image" style={bImage} onClick={() => openRecipe(false)} />
            <div className="recipe-detail-box">
              <div className="full-recipe-title" style={tColor}>{`${recipe.title || ''} ${recipe.subtitle || ''}`}</div>
              <div className="recipe-rating">
                <span className={stars > 4 ? 'give-star' : ''} data-star="5" onClick={this.giveRating} style={tColor}>&#9734;</span>
                <span className={stars > 3 ? 'give-star' : ''} data-star="4" onClick={this.giveRating} style={tColor}>&#9734;</span>
                <span className={stars > 2 ? 'give-star' : ''} data-star="3" onClick={this.giveRating} style={tColor}>&#9734;</span>
                <span className={stars > 1 ? 'give-star' : ''} data-star="2" onClick={this.giveRating} style={tColor}>&#9734;</span>
                <span className={stars > 0 ? 'give-star' : ''} data-star="1" onClick={this.giveRating} style={tColor}>&#9734;</span>
              </div>
              <button style={button} className="made-this" onClick={this.madeThis}>I made this</button>
              <button className="share-button" onClick={this.print}>Print</button>
              <X className="close-tab" style={iconStyle} onClick={() => openRecipe(false)} />
            </div>
          </div>
          { notes ? NotesHolder : RecipeBody }
        </div>
      </div>
    );
  }
}

RecipeFull.propTypes = {
  recipe: PropTypes.object,
  color: PropTypes.string,
  day: PropTypes.string,
  openRecipe: PropTypes.func,
  changeRecipe: PropTypes.func,
};

RecipeFull.defaultProps = {
  recipe: {},
  color: '',
};
