import React from 'react';
import PropTypes from 'prop-types';

export default class NutrientOfTheWeek extends React.Component {
  render() {
    const { week } = this.props;
    const bColor = {
      backgroundColor: week.color || '',
    };
    const color = {
      color: week.color || '',
    };
    const nutrientImage = week.nutrientImage && week.nutrientImage.fields ? week.nutrientImage.fields.file.url : '';
    return (
      <div className="week-nutrient">
        <div className="week-nutrient-left-pane" style={bColor}>
          <h2 className="nutrient-of-week">
            This week&rsquo;s nutrient is {week.nutrient}.
          </h2>
          {nutrientImage ? <img src={nutrientImage} className="nutrient-image" alt="Nutrient Image" /> : ''}
        </div>
        <div className="week-nutrient-right-pane">
          <p style={color}>{week.nutrientDescription}</p>
        </div>
      </div>
    )
  }
}

NutrientOfTheWeek.propTypes = {
  week: PropTypes.object,
};
