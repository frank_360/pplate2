import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { createClient } from 'contentful';
import { Meteor } from 'meteor/meteor';
import _ from 'underscore';
import { ChevronRight, ChevronLeft, ChevronDown, ChevronUp, X } from 'react-feather';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

export default class WeekButtons extends React.Component {
  constructor(props) {
    super(props);
    this.getTiles = this.getTiles.bind(this);
    this.goToWeek = this.goToWeek.bind(this);
    this.goBack = this.goBack.bind(this);
    this.goForward = this.goForward.bind(this);
    this.state = {
      weeks: {},
      trimester: 1,
      screen: 1,
      open: false,
    };
  }
  componentWillMount() {
    const self = this;
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    client.getEntries({
      content_type: 'week',
      include: 0,
    }).then((entries) => {
      if (entries.items && entries.items[0]) {
        const weeks = {};
        _.each(entries.items, (e) => {
          weeks[e.fields.number] = e.fields.color;
        });
        self.setState({
          weeks,
        });
      }
    });
  }
  getTiles(low, high) {
    const { weeks, open } = this.state;
    return _.map(_.range(low, high), (n) => {
      const color = weeks ? weeks[n] : '';
      const lColor = {
        backgroundColor: color ? shadeColor(color, 0.40) : '',
      };
      const tColor = {
        color: color || '',
      };
      const bColor = {
        backgroundColor: color || '',
      };
      return (
        <div className="calendar-rec" key={n} data-week={n} onClick={this.goToWeek}>
          <div className="calendar-top" style={lColor}>
            <div className="calendar-hole" style={bColor} />
            <div className="calendar-hole calendar-hole-right" style={bColor} />
            <span className="calendar-day" style={tColor}>Week</span>
          </div>
          <div className="calendar-bottom" style={bColor}><span className="big-number">{n}</span></div>
        </div>
      );
    });
  }
  goToWeek(e) {
    const week = e.currentTarget.dataset.week;
    window.location.href = `/dashboard?week=${week}`;
  }
  goForward() {
    const week = this.props.weekNumber + 1;
    window.location.href = `/dashboard?week=${week}`;
  }
  goBack() {
    const week = this.props.weekNumber - 1;
    window.location.href = `/dashboard?week=${week}`;
  }
  render() {
    const { week, weekNumber } = this.props;
    const { trimester, screen, open } = this.state;
    const bColor = {
      backgroundColor: week.color || '',
    };
    let trimesterN = '1st';
    if (trimester === 2) trimesterN = '2nd';
    else if (trimester === 3) trimesterN = '3rd';
    const fourtotwelve = this.getTiles(4, 13);
    const thirteentotwentyone = this.getTiles(13, 22);
    const twentytwototwentyeight = this.getTiles(22, 29);
    const twentyninetothirtyseven = this.getTiles(29, 38);
    const thirtyeighttoforty = this.getTiles(38, 41);
    const FirstTrimester = (
      <div className="trimester-tiles">
        {fourtotwelve}
        <ChevronRight className="right-arrow trimester-arrow" onClick={() => this.setState({ trimester: 2, screen: 1 })} />
      </div>
    );
    const SecondTrimester = (
      <div className="trimester-tiles">
        <ChevronLeft className="left-arrow trimester-arrow" onClick={() => this.setState({ trimester: 1, screen: 1 })} />
        {screen === 1 ? thirteentotwentyone : twentytwototwentyeight }
        <ChevronRight className="right-arrow trimester-arrow" onClick={() => this.setState({ trimester: 3, screen: 1 })} />
        {screen === 2 ?
          <ChevronUp className="screen-arrow up-arrow" onClick={() => this.setState({ screen: 1 })} /> :
          <ChevronDown className="screen-arrow down-arrow" onClick={() => this.setState({ screen: 2 })} />
        }
      </div>
    );
    const ThirdTrimester = (
      <div className="trimester-tiles">
        <ChevronLeft className="left-arrow trimester-arrow" onClick={() => this.setState({ trimester: 2, screen: 1 })} />
        {screen === 1 ? twentyninetothirtyseven : thirtyeighttoforty }
        {screen === 2 ?
          <ChevronUp className="screen-arrow up-arrow" onClick={() => this.setState({ screen: 1 })} /> :
          <ChevronDown className="screen-arrow down-arrow" onClick={() => this.setState({ screen: 2 })} />
        }
      </div>
    );
    return (
      <div className="standard-container week-group">
        { open ?
          <div className="big-week-selector">
            <div className="background-blur" />
            <span style={bColor} className="week-button" onClick={() => this.setState({ open: false })}>Week {weekNumber}  {open ? <X className="close-tab" /> : ''}</span>
            <span style={bColor} className="trimester-label">{trimesterN} Trimester</span>
            { trimester === 1 ? FirstTrimester : ''}
            { trimester === 2 ? SecondTrimester : ''}
            { trimester === 3 ? ThirdTrimester : ''}
          </div>
          :
          <div className="week-buttons">
            <span style={bColor} className={weekNumber !== 4 ? 'week-button' : 'week-button invisible'} onClick={this.goBack}>Last Week</span>
            <span style={bColor} className="week-button" onClick={() => this.setState({ open: true })}>Week {weekNumber}</span>
            <span style={bColor} className={weekNumber !== 40 ? 'week-button' : 'week-button invisible'} onClick={this.goForward}>Next Week</span>
          </div>
        }
      </div>
    );
  }
}

WeekButtons.propTypes = {
  weekNumber: PropTypes.number,
  week: PropTypes.object,
};
