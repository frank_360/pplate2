import React from 'react';
import { Link } from 'react-router';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { createClient } from 'contentful';
import _ from 'underscore';
import RecipeSwappable from './RecipeSwappable.js';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

export default class RecommendedSwaps extends React.Component {
  constructor(props) {
    super(props);
    this.getData = this.getData.bind(this);
    this.withId = this.withId.bind(this);
    this.state = {
      loading: true,
      recipes: [],
    };
  }
  componentWillMount() {
    this.getData(this.props);
  }
  componentWillReceiveProps(nextProps) {
    const { weekNumber } = this.props;
    if (nextProps.weekNumber !== weekNumber) {
      this.setState({ loading: true });
      this.getData(nextProps);
    }
  }
  getData(props) {
    const { weekNumber } = props;
    const self = this;
    const client = createClient({
      space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
      accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
    });
    let trimester = 1;
    if (weekNumber > 12) trimester = 2;
    else if (weekNumber > 28) trimester = 3;
    client.getEntries({
      content_type: 'trimester',
      'fields.number': trimester,
      include: 1,
    }).then((entries) => {
      if (entries.items && entries.items[0]) {
        const recipes = _.chain(entries.items[0].fields.recipes)
          .map(r => self.withId(r))
          .shuffle()
          .first(5)
          .value();
        self.setState({ recipes });
      } else self.setState({ recipes: [] });
      self.setState({ loading: false });
    });
  }
  withId(recipe) {
    const recipeId = recipe.sys.id;
    return _.extend(recipe.fields, {
      id: recipeId,
    });
  }
  render() {
    const { loading, recipes } = this.state;
    if (loading) return <div className="recommended-swaps" />
    const { color, weekNumber, swapped } = this.props;
    const bColor = {
      backgroundColor: color,
    };
    const tColor = {
      color,
    };
    const button = {
      color,
      backgroundColor: color ? shadeColor(color, 0.70) : '',
    };
    return (
      <div className="recommended-swaps">
        <span className="rec-header" style={bColor}>Recommended Swaps</span>
        <div className="swaps-row">
          <RecipeSwappable recipe={recipes[0]} color={color} weekNumber={weekNumber} swapped={swapped} />
          <RecipeSwappable recipe={recipes[1]} color={color} weekNumber={weekNumber} swapped={swapped} />
          <RecipeSwappable recipe={recipes[2]} color={color} weekNumber={weekNumber} swapped={swapped} />
          <RecipeSwappable recipe={recipes[3]} color={color} weekNumber={weekNumber} swapped={swapped} />
          <RecipeSwappable recipe={recipes[4]} color={color} weekNumber={weekNumber} swapped={swapped} />
        </div>
        <Link to="#" className="browse-recipes-button" style={button}>Browse All Recipes</Link>
      </div>
    );
  }
}

RecommendedSwaps.propTypes = {
  weekNumber: PropTypes.number,
  color: PropTypes.string,
  swapped: PropTypes.func,
};

RecommendedSwaps.defaultProps = {
  color: '',
};
