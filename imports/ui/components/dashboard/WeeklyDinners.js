import React from "react";
import { Meteor } from "meteor/meteor";
import PropTypes from "prop-types";
import Loading from "/imports/ui/components/general/Loading.js";
import _ from "underscore";
import RecipeBlock from "./RecipeBlock.js";
import RecipeFull from "./RecipeFull.js";

function shadeColor(color, percent) {
  const f = parseInt(color.slice(1), 16),
    t = percent < 0 ? 0 : 255,
    p = percent < 0 ? percent * -1 : percent,
    R = f >> 16,
    G = (f >> 8) & 0x00ff,
    B = f & 0x0000ff;
  return (
    "#" +
    (
      0x1000000 +
      (Math.round((t - R) * p) + R) * 0x10000 +
      (Math.round((t - G) * p) + G) * 0x100 +
      (Math.round((t - B) * p) + B)
    )
      .toString(16)
      .slice(1)
  );
}

function arraysEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;
  for (var i = arr1.length; i--; ) {
    if (arr1[i] !== arr2[i]) return false;
  }

  return true;
}

export default class WeeklyDinners extends React.Component {
  constructor(props) {
    super(props);
    this.openRecipe = this.openRecipe.bind(this);
    this.changeRecipe = this.changeRecipe.bind(this);
    this.state = {
      open: false,
      recipes: {},
      loading: false
    };
  }
  componentWillMount() {
    this.setState({ recipes: this.props.recipes });
  }
  componentWillReceiveProps(nextProps) {
    // const isSwap = !arraysEqual(_.pluck(nextProps.recipes, 'swap'), _.pluck(this.props.recipes, 'swap'));
    const isSwap = nextProps.num !== this.props.num;
    if (isSwap) {
      const self = this;
      this.setState({ loading: true, recipes: nextProps.recipes });
      Meteor.setTimeout(() => {
        self.setState({ loading: false });
      }, 500);
    }
  }
  openRecipe(open) {
    this.setState({ open }, () => {
      document.getElementById("weekd").scrollIntoView();
    });
  }
  changeRecipe(day, recipeNew) {
    const { recipes } = this.state;
    recipes[day] = recipeNew;
    this.setState({ recipes });
  }
  render() {
    const { color, swapped, weekNumber } = this.props;
    const { open, recipes, loading } = this.state;
    const bColor = {
      backgroundColor: color ? shadeColor(color, 0.7) : ""
    };
    const tColor = {
      color: color || ""
    };
    if (loading) {
      return (
        <div className="weekly-dinners weekly-dinners-spin" style={bColor}>
          <h2 style={tColor}>Here are your dinners this week</h2>
          <Loading />
        </div>
      );
    }
    const recipe = open ? recipes[open] : {};
    return (
      <div
        className={open ? "weekly-dinners big-time-dinner" : "weekly-dinners"}
        style={bColor}
        id="weekd"
      >
        <h2 style={tColor}>Here are your dinners this week</h2>
        <div className="standard-container big-dinner-row">
          {open ? (
            <RecipeFull
              recipe={recipe}
              color={color}
              day={open}
              openRecipe={this.openRecipe}
              changeRecipe={this.changeRecipe}
            />
          ) : (
            ""
          )}
          <div className={open ? "dinner-row small-row" : "dinner-row"}>
            {open === "sunday" ? (
              ""
            ) : (
              <RecipeBlock
                day="sunday"
                color={color}
                recipe={recipes.sunday}
                openRecipe={this.openRecipe}
                changeRecipe={this.changeRecipe}
                swapped={swapped}
                weekNumber={weekNumber}
              />
            )}
            {open === "monday" ? (
              ""
            ) : (
              <RecipeBlock
                day="monday"
                color={color}
                recipe={recipes.monday}
                openRecipe={this.openRecipe}
                changeRecipe={this.changeRecipe}
                swapped={swapped}
                weekNumber={weekNumber}
              />
            )}
            {open === "tuesday" ? (
              ""
            ) : (
              <RecipeBlock
                day="tuesday"
                color={color}
                recipe={recipes.tuesday}
                openRecipe={this.openRecipe}
                changeRecipe={this.changeRecipe}
                swapped={swapped}
                weekNumber={weekNumber}
              />
            )}
            {open === "wednesday" ? (
              ""
            ) : (
              <RecipeBlock
                day="wednesday"
                color={color}
                recipe={recipes.wednesday}
                openRecipe={this.openRecipe}
                changeRecipe={this.changeRecipe}
                swapped={swapped}
                weekNumber={weekNumber}
              />
            )}
            {open === "thursday" ? (
              ""
            ) : (
              <RecipeBlock
                day="thursday"
                color={color}
                recipe={recipes.thursday}
                openRecipe={this.openRecipe}
                changeRecipe={this.changeRecipe}
                swapped={swapped}
                weekNumber={weekNumber}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

WeeklyDinners.propTypes = {
  recipes: PropTypes.object,
  color: PropTypes.string,
  swapped: PropTypes.func,
  weekNumber: PropTypes.number,
  num: PropTypes.number
};

WeeklyDinners.defaultProps = {
  recipes: {},
  color: ""
};
