import Radium from 'radium';
import React from 'react';
import PropTypes from 'prop-types';
import { RefreshCw, ChevronDown } from 'react-feather';
import { newSwap } from '/imports/api/swaps/methods.js';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

class RecipeSwappable extends React.Component {
  constructor(props) {
    super(props);
    this.handleSwap = this.handleSwap.bind(this);
    this.state = {
      swapMode: false,
    };
  }
  handleSwap(e) {
    const self = this;
    const { recipe, weekNumber, swapped } = this.props;
    const day = e.currentTarget.dataset.day;
    newSwap.call({ week: weekNumber, day, recipeId: recipe.id }, (err) => {
      if (!err) {
        self.setState({ swapMode: false });
        swapped();
      }
    });
  }
  render() {
    const { recipe, color } = this.props;
    const { swapMode } = this.state;
    const bImage = {
      backgroundColor: color,
      backgroundImage: recipe.image ? `url(${recipe.image.fields.file.url})` : '',
    };
    const tColor = {
      color,
    };
    const bigCircle = {
      backgroundColor: color ? shadeColor(color, 0.70) : '',
      borderColor: color,
    };
    const iconStyle = {
      stroke: color,
      borderColor: color,
    };
    const hColor = {
      backgroundColor: color ? shadeColor(color, 0.70) : '',
      color,
    };
    const swapColor = {
      borderColor: color,
      color,
      ':hover': {
        backgroundColor: color,
        color: 'white',
      },
    };
    const Swapper = (
      <div className="swapper">
        <div className="swapper-header" style={hColor}>
          Swap With
          <ChevronDown className="close-swapper" style={iconStyle} onClick={() => this.setState({ swapMode: false })} />
        </div>
        <div className="swapper-body">
          <div style={swapColor} key={`${recipe.id}s`} className="swap-day" data-day="sunday" onClick={this.handleSwap}>Sunday</div>
          <div style={swapColor} key={`${recipe.id}m`} className="swap-day" data-day="monday" onClick={this.handleSwap}>Monday</div>
          <div style={swapColor} key={`${recipe.id}tu`} className="swap-day" data-day="tuesday" onClick={this.handleSwap}>Tuesday</div>
          <div style={swapColor} key={`${recipe.id}w`} className="swap-day" data-day="wednesday" onClick={this.handleSwap}>Wednesday</div>
          <div style={swapColor} key={`${recipe.id}th`} className="swap-day" data-day="thursday" onClick={this.handleSwap}>Thursday</div>
        </div>
      </div>
    );
    return (
      <div className="recipe-swappable">
        {swapMode ? Swapper : ''}
        <div className="rs-circle" style={bigCircle}>
          <div className="res-inner-circle" style={bImage} />
          <RefreshCw className="swap-icon" style={iconStyle} onClick={() => this.setState({ swapMode: true })} />
        </div>
        <div className="rs-details">
          <span className="rs-details-title" style={tColor}>{recipe.title}</span>
          <span className="rs-details-subtitle" style={tColor}>{recipe.subtitle}</span>
        </div>
      </div>
    );
  }
}

RecipeSwappable.propTypes = {
  recipe: PropTypes.object,
  weekNumber: PropTypes.number,
  color: PropTypes.string,
  swapped: PropTypes.func,
};

RecipeSwappable.defaultProps = {
  recipe: {},
  color: '',
};

Layout = Radium(RecipeSwappable);
export default Layout;
