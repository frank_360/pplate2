import React from 'react';
import PropTypes from 'prop-types';
import Radium from 'radium';
import { newRating, madeThisToggle, isMade } from '/imports/api/ratings/methods.js';
import { X } from 'react-feather';
import { deleteSwap } from '/imports/api/swaps/methods.js';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

class RecipeBlock extends React.Component {
  constructor(props) {
    super(props);
    this.giveRating = this.giveRating.bind(this);
    this.madeThis = this.madeThis.bind(this);
    this.removeSwap = this.removeSwap.bind(this);
  }
  componentWillMount() {
    const { recipe } = this.props;
    isMade.call({ recipeId: recipe.id }, (err, res) => {
      this.setState({ madeThis: res });
    });
  }
  giveRating(e) {
    const { recipe, changeRecipe, day } = this.props;
    const stars = parseInt(e.currentTarget.dataset.star, 10);
    newRating.call({ recipeId: recipe.id, stars }, (err) => {
      if (!err) {
        recipe.stars = stars;
        changeRecipe(day, recipe);
      }
    });
  }
  madeThis() {
    const { recipe } = this.props;
    const { madeThis } = this.state;
    const self = this;
    madeThisToggle.call({ recipeId: recipe.id, made: madeThis }, (err) => {
      console.log(err);
      if (!err) {
        self.setState({ madeThis: !madeThis })
      }
    });
  }
  removeSwap() {
    const { day, weekNumber, swapped } = this.props;
    deleteSwap.call({ week: weekNumber, day }, (err) => {
      if (!err) swapped();
    });
  }
  render() {
    const { day, color, recipe, openRecipe } = this.props;
    const { stars } = recipe;
    const { madeThis } = this.state;
    const bColor = {
      backgroundColor: color || '',
    };
    const lColor = {
      backgroundColor: color ? shadeColor(color, 0.40) : '',
    };
    const tColor = {
      color: color || '',
    };
    const bImage = {
      backgroundColor: color || '',
      backgroundImage: recipe.image ? `url(${recipe.image.fields.file.url})` : '',
    };
    const button = {
      borderColor: color,
      color: madeThis ? 'white' : color,
      backgroundColor: madeThis ? color : 'white',
    };
    const iconStyle = {
      stroke: color,
      borderColor: color,
    };
    return (
      <div className="recipe-block">
        {recipe.swap ?
          <X title="Remove Swap" className="undo-swap" style={iconStyle} onClick={this.removeSwap} />
          : ''}
        <div className="calendar-rec">
          <div className="calendar-top" style={lColor}>
            <div className="calendar-hole" style={bColor} />
            <div className="calendar-hole calendar-hole-right" style={bColor} />
            <span className="calendar-day" style={tColor}>{day}</span>
          </div>
          <div className="calendar-bottom" style={bImage} onClick={() => openRecipe(day)} />
        </div>
        <div className="recipe-primary-info">
          <span className="recipe-title" style={tColor}>{recipe.title}</span>
          <span className="recipe-subtitle" style={tColor}>{recipe.subtitle}</span>
          <div className="recipe-rating">
            <span className={stars > 4 ? 'give-star' : ''} data-star="5" onClick={this.giveRating} style={tColor}>&#9734;</span>
            <span className={stars > 3 ? 'give-star' : ''} data-star="4" onClick={this.giveRating} style={tColor}>&#9734;</span>
            <span className={stars > 2 ? 'give-star' : ''} data-star="3" onClick={this.giveRating} style={tColor}>&#9734;</span>
            <span className={stars > 1 ? 'give-star' : ''} data-star="2" onClick={this.giveRating} style={tColor}>&#9734;</span>
            <span className={stars > 0 ? 'give-star' : ''} data-star="1" onClick={this.giveRating} style={tColor}>&#9734;</span>
          </div>
          <button className="made-this" style={button} onClick={this.madeThis}>I made this</button>
        </div>
      </div>
    );
  }
}

RecipeBlock.propTypes = {
  day: PropTypes.string,
  recipe: PropTypes.object,
  color: PropTypes.string,
  openRecipe: PropTypes.func,
  changeRecipe: PropTypes.func,
  swapped: PropTypes.func,
  weekNumber: PropTypes.number,
};

RecipeBlock.defaultProps = {
  recipe: {},
};

Layout = Radium(RecipeBlock);
export default Layout;
