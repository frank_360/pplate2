import React from 'react';
import PropTypes from 'prop-types';

function shadeColor(color, percent) {
  const f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
  return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

export default class WeeklyTips extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTip: 'weeklyTip',
    };
  }
  render() {
    const { tips, color } = this.props;
    const { activeTip } = this.state;
    const bColor = {
      backgroundColor: color,
    };
    const tColor = {
      color,
    };
    const text = tips[activeTip];
    const imageStyle = {
      backgroundImage: `url(${tips[`${activeTip}Image`]})`,
      backgroundColor: color,
    };
    const innerColor = {
      backgroundColor: color ? shadeColor(color, 0.70) : '',
      borderColor: color,
    };
    const wColor = {
      backgroundColor: color ? shadeColor(color, 0.9) : '',
    };
    const WeeklyTip = (
      <div className="vertical-tip">
        {/* <div className="rs-circle big-rs-circle" style={innerColor}>
          <div className="res-inner-circle big-res-inner" style={imageStyle} />
        </div> */}
        <span className="tip-big-text" style={tColor}>{text}</span>
      </div>
    );
    const MealTip = (
      <div className="horizontal-tip">
        {/* <div className="rs-circle big-rs-circle" style={innerColor}>
          <div className="res-inner-circle big-res-inner" style={imageStyle} />
        </div> */}
        <span className="tip-small-text" style={tColor}>{text}</span>
      </div>
    );
    const weekStyle = {
      backgroundColor: activeTip === 'weeklyTip' ? color : '',
      color: activeTip === 'weeklyTip' ? 'white' : color,
      borderColor: color,
    };
    const breakfastStyle = {
      backgroundColor: activeTip === 'breakfastTip' ? color : '',
      color: activeTip === 'breakfastTip' ? 'white' : color,
      borderColor: color,
    };
    const lunchStyle = {
      backgroundColor: activeTip === 'lunchTip' ? color : '',
      color: activeTip === 'lunchTip' ? 'white' : color,
      borderColor: color,
    };
    const snackStyle = {
      backgroundColor: activeTip === 'snackTip' ? color : '',
      color: activeTip === 'snackTip' ? 'white' : color,
      borderColor: color,
    };
    return (
      <div className="weekly-tips" style={wColor}>
        <div className="tip-menu">
          <span style={weekStyle} className={activeTip === 'weeklyTip' ? 'wtm-option active-option' : 'wtm-option'} onClick={() => this.setState({ activeTip: 'weeklyTip' })}>Weekly Tip</span>
          <span style={breakfastStyle} className={activeTip === 'breakfastTip' ? 'wtm-option active-option' : 'wtm-option'} onClick={() => this.setState({ activeTip: 'breakfastTip' })}>Breakfast Tip</span>
          <span style={lunchStyle} className={activeTip === 'lunchTip' ? 'wtm-option active-option' : 'wtm-option'} onClick={() => this.setState({ activeTip: 'lunchTip' })}>Lunch Tip</span>
          <span style={snackStyle} className={activeTip === 'snackTip' ? 'wtm-option active-option' : 'wtm-option'} onClick={() => this.setState({ activeTip: 'snackTip' })}>Snack Tip</span>
        </div>
        <div className="tip-content">
          {activeTip === 'weeklyTip' ? WeeklyTip : MealTip}
        </div>
      </div>
    );
  }
}

WeeklyTips.propTypes = {
  tips: PropTypes.object,
  color: PropTypes.string,
};

WeeklyTips.defaultProps = {
  tips: {},
  color: '',
};
