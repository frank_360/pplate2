import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { updateMembership } from '/imports/api/users/methods.js';

export default class MembershipOptions extends React.Component {
  constructor(props) {
    super(props);

    if (props.title) {
      this.state = { ...props };
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.title) {
      // updateMembership.call({})
      this.setState({ ...nextProps });
    }
  }
  render() {
    let one = '';
    let two = '';
    let three = '';
    let four = '';

    const hideTrialBtn = this.props.hideTrial ? 'hidden' : '';
    const customer = Meteor.user();

    if (this.props.price || this.props.price === 0) {
      switch (this.props.price) {
        case 0:
          one = 'selected';
          break;
        case 1800:
          two = 'selected';
          break;
        case 5000:
          three = 'selected';
          break;
        case 9000:
          four = 'selected';
          break;
        default:
          return true;
      }
    } else if (this.props.prevPath === '/' && this.location.state && this.location.state.membership) {
      switch (this.location.state.membership.price) {
        case 0:
          one = 'selected';
          break;
        case 1800:
          two = 'selected';
          break;
        case 5000:
          three = 'selected';
          break;
        case 9000:
          four = 'selected';
          break;
        default:
          return true;
      }
    }

    const LoggedInOptions = (
      <div className="sign-up-card-container">
        <Link
          to={{
            pathname: '/account-upgrade',
            state: {
              membership: {
                title: '1 Month',
                price: 1800,
                frequency: 'Recurring Payment',
              },
            },
          }}
          className={`sign-up-membership-card ${two}`}
        >
          <p className="card-title">1 Month</p>
          <p className="card-price">$18</p>
          <p className="card-frequency">Recurring Payment</p>
        </Link>
        <Link
          to={{
            pathname: '/account-upgrade',
            state: {
              membership: {
                title: '3 Months',
                price: 5000,
                frequency: 'One-time payment',
              },
            },
          }}
          className={`sign-up-membership-card ${three}`}
        >
          <p className="card-title">3 Months</p>
          <p className="card-price">$50</p>
          <p className="card-frequency">One-time Payment</p>
        </Link>
        <Link
          to={{
            pathname: '/account-upgrade',
            state: {
              membership: {
                title: '6 Months',
                price: 9000,
                frequency: 'One-time payment',
              },
            },
          }}
          className={`sign-up-membership-card ${four}`}
        >
          <p className="card-title">6 Months</p>
          <p className="card-price">$90</p>
          <p className="card-frequency">One-time Payment</p>
        </Link>
      </div>
    );

    const LoggedOutOptions = (
      <div className="sign-up-card-container">
        <Link
          to={{
            pathname: '/signup',
            state: {
              membership: {
                title: '2-Week Free Trial',
                price: 0,
                frequency: '$18/month after trial',
              },
              prevPath: this.props.prevPath,
            },
          }}
          className={`sign-up-membership-card ${one} ${hideTrialBtn}`}
        >
          <p className="card-title">2-Week</p>
          <p className="card-price ft">Free Trial</p>
          <p className="card-frequency">$18/month after trial<br />(CC Required)</p>
        </Link>
        <Link
          to={{
            pathname: '/signup',
            state: {
              membership: {
                title: '1 Month',
                price: 1800,
                frequency: 'Recurring Payment',
              },
            },
          }}
          className={`sign-up-membership-card ${two}`}
        >
          <p className="card-title">1 Month</p>
          <p className="card-price">$18</p>
          <p className="card-frequency">Recurring<br />Payment</p>
        </Link>
        <Link
          to={{
            pathname: '/signup',
            state: {
              membership: {
                title: '3 Months',
                price: 5000,
                frequency: 'One-time payment',
              },
            },
          }}
          className={`sign-up-membership-card ${three}`}
        >
          <p className="card-title">3 Months</p>
          <p className="card-price">$50</p>
          <p className="card-frequency">One-time<br />Payment</p>
        </Link>
        <Link
          to={{
            pathname: '/signup',
            state: {
              membership: {
                title: '6 Months',
                price: 9000,
                frequency: 'One-time payment',
              },
            },
          }}
          className={`sign-up-membership-card ${four}`}
        >
          <p className="card-title">6 Months</p>
          <p className="card-price">$90</p>
          <p className="card-frequency">One-time<br />Payment</p>
        </Link>
      </div>
    );
    return (
      <div>
        {customer ? LoggedInOptions : LoggedOutOptions }
      </div>
    );
  }
}

MembershipOptions.propTypes = {
  price: PropTypes.number,
  prevPath: PropTypes.string,
};
