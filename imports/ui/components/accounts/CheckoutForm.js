import React from 'react';
import { browserHistory } from 'react-router';
import { injectStripe, CardElement } from 'react-stripe-elements';
import Loading from '/imports/ui/components/general/Loading.js';
import { purchaseSub } from '/imports/api/users/methods.js';
import { createClient } from 'contentful';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      amount: props.amount,
      dollarsOff: 0,
    };
  }
  componentWillMount() {
    const user = Meteor.user();
    const coupon = user.profile.couponCode;
    if (coupon) {
      const client = createClient({
        space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
        accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
      });
      client.getEntries({
        content_type: 'coupons',
        'fields.code': coupon,
      }).then((entries) => {
        if (entries.total) {
          const amount = 12900 - (entries.items[0].fields.dollarsOff * 100);
          this.setState({ amount });
        }
      });
    }
  }
  handleSubmit = (ev) => {
    ev.preventDefault();
    const { amount } = this.state;
    const user = Meteor.user();
    const self = this;
    self.setState({ loading: true });

    const upgrade = this.props.upgrade || false;

    if (upgrade === true) {
      this.props.stripe.createToken().then(({ token }) => {
        if (!token) return self.setState({ loading: false });
        purchaseSub.call({
          token,
          amount: self.state.amount,
          membership: this.props.membership,
          user,
          upgrade,
        }, (err, res) => {
          if (!err) {
            window.location.href = '/dashboard';
          } else {
            console.log(err);
            self.setState({ loading: false });
          }
        });
      });
    } else {
      this.props.stripe.createToken().then(({ token }) => {
        if (!token) return self.setState({ loading: false });
        purchaseSub.call({
          token,
          amount: self.state.amount,
          membership: user.profile.membership,
          user,
          upgrade
        }, (err, res) => {
          if (!err) {
            window.location.href = '/dashboard';
          } else {
            console.log(err);
            self.setState({ loading: false });
          }
        });
      });
    }
  }
  render() {
    const { loading, amount } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <CardElement />
        {loading ? <div className="payment-loading"><Loading /></div> : <button className="continueButton">{amount ? 'Confirm order' : 'Start 2-week Free Trial'}</button> }
        <a href="https://www.stripe.com" className="stripe-badge" target="_blank"><img src="/images/stripe.svg"alt="" /></a>
        <p className="secure-checkout">100% secure checkout</p>
        <p className="extra-details">Free trial subscribers will be enrolled in the monthly subscription and charged at the end of the trial period.</p>
        <div className="social-separator" />
        <p className="extra-details">Monthly subscription will auto-renew monthly. Your last monthly charge will be for the 30 days within your due date based on your billing cycle. You will not be charged after your due date.</p>
        <div className="social-separator" />
        <p className="extra-details">3 month and 6 month memberships are a one-time charge.</p>
        <div className="social-separator" />
        <p className="extra-details">You can change your membership plan at any time. Cancel your monthly membership or free trial through your account page or email <a href="#">subscriptions@pregnancyplate.com</a>.</p>
      </form>
    );
  }
}

CheckoutForm.propTypes = {
  amount: PropTypes.number,
};

export default injectStripe(CheckoutForm);
