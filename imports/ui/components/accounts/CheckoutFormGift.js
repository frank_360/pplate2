import React from 'react';
import { injectStripe, CardElement } from 'react-stripe-elements';
import Loading from '/imports/ui/components/general/Loading.js';
import { getCoupon, purchaseGift } from '/imports/api/coupons/methods.js';
import { createClient } from 'contentful';
import { Meteor } from 'meteor/meteor';
import swal from 'sweetalert';

class CheckoutForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      amount: props.amount,
      dollarsOff: 0,
    };
  }
  componentWillMount() {
    getCoupon.call({ code: window.location.pathname.split('/').pop() }, (err, coupon) => {
      const couponCode = coupon.profile.couponCode;
      if (couponCode) {
        const client = createClient({
          space: Meteor.settings.public.CONTENTFUL_SPACE_ID,
          accessToken: Meteor.settings.public.CONTENTFUL_ACCESS_TOKEN,
        });
        client.getEntries({
          content_type: 'coupons',
          'fields.code': couponCode,
        }).then((entries) => {
          if (entries.total) {
            const amount = 12900 - (entries.items[0].fields.dollarsOff * 100);
            this.setState({ amount });
          }
        });
      }
    });
  }
  handleSubmit = (ev) => {
    ev.preventDefault();
    const { amount } = this.state;
    if (amount === 0) {
      return purchaseGift.call({
        token: {},
        amount,
        code,
      }, (err, res) => {
        if (!err) {
          swal({
            title: 'Purchased!',
            text: 'Check your email for confirmation of your purchase',
            icon: 'success',
            button: 'Continue',
          }).then((confirm) => {
            if (confirm) window.location.href = '/';
          });
        }
      });
    }
    const self = this;
    self.setState({ loading: true });
    this.props.stripe.createToken().then(({ token }) => {
      if (!token) return self.setState({ loading: false });
      purchaseGift.call({
        token,
        amount: self.state.amount,
        code: window.location.pathname.split('/').pop()
      }, (err, res) => {
        if (!err) {
          swal({
            title: 'Purchased!',
            text: 'Check your email for confirmation of your purchase',
            icon: 'success',
            button: 'Continue',
          }).then((confirm) => {
            if (confirm) window.location.href = '/';
          });
        }
      });
    });
  }
  render() {
    const { loading, amount } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <CardElement />
        { loading ? <div className="payment-loading"><Loading /></div> : <button className="continueButton">{amount ? 'Confirm order' : 'Claim Free Gift'}</button> }
        <a href="https://www.stripe.com" className="stripe-badge" target="_blank"><img src="/images/stripe.svg"alt="" /></a>
        <p className="secure-checkout">100% secure checkout</p>
      </form>
    );
  }
}

export default injectStripe(CheckoutForm);
