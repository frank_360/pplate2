import Radium from 'radium';
import React from 'react';
import PropTypes from 'prop-types';

class AllRecipeBlock extends React.Component {
  render() {
    const { recipe, show } = this.props;
    const stars = recipe.stars;
    const bImage = {
      backgroundImage: recipe.image ? `url(${recipe.image.fields.file.url})` : '',
    };
    return (
      <div className="recipe-swappable" onClick={() => show(recipe.id)}>
        <div className="rs-circle">
          <div className="res-inner-circle" style={bImage} />
        </div>
        <div className="rs-details">
          <span className="rs-details-title">{recipe.title}</span>
          <span className="rs-details-subtitle">{recipe.subtitle}</span>
        </div>
        {stars ?
          <div className="recipe-rating">
            <span className={stars > 4 ? 'give-star' : ''} data-star="5">&#9734;</span>
            <span className={stars > 3 ? 'give-star' : ''} data-star="4">&#9734;</span>
            <span className={stars > 2 ? 'give-star' : ''} data-star="3">&#9734;</span>
            <span className={stars > 1 ? 'give-star' : ''} data-star="2">&#9734;</span>
            <span className={stars > 0 ? 'give-star' : ''} data-star="1">&#9734;</span>
          </div>
          : ''}
      </div>
    );
  }
}

AllRecipeBlock.propTypes = {
  recipe: PropTypes.object,
  show: PropTypes.func,
};

AllRecipeBlock.defaultProps = {
  recipe: {},
};

Layout = Radium(AllRecipeBlock);
export default Layout;
