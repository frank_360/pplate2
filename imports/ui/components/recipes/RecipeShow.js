import React from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import marked from 'marked';
import { newRating, madeThisToggle } from '/imports/api/ratings/methods.js';
import { getNote, updateNotes } from '/imports/api/notes/methods.js';
import { X } from 'react-feather';

export default class RecipeShow extends React.Component {
  constructor(props) {
    super(props);
    this.giveRating = this.giveRating.bind(this);
    this.madeThis = this.madeThis.bind(this);
    this.handleNotes = this.handleNotes.bind(this);
    this.state = {
      notes: false,
      note: '',
    };
  }
  componentWillMount() {
    const { recipe } = this.props;
    const self = this;
    getNote.call({ recipeId: recipe.id }, (err, res) => {
      if (res) self.setState({ note: res.text });
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.recipe.id !== this.props.recipe.id) {
      this.setState({ note: '' });
      const self = this;
      getNote.call({ recipeId: nextProps.recipe.id }, (err, res) => {
        if (res) self.setState({ note: res.text });
      });
    }
  }
  giveRating(e) {
    const { recipe } = this.props;
    const stars = parseInt(e.currentTarget.dataset.star, 10);
    newRating.call({ recipeId: recipe.id, stars }, (err) => {
      if (!err) {
        recipe.stars = stars;
      }
    });
  }
  madeThis() {
    const { recipe } = this.props;
    madeThisToggle.call({ recipeId: recipe.id, made: !recipe.madeThis }, (err) => {
      if (!err) {
        recipe.madeThis = !recipe.madeThis;
      }
    });
  }
  handleNotes(e) {
    const { recipe } = this.props;
    const value = e.currentTarget.value;
    this.setState({ note: value });
    updateNotes.call({ recipeId: recipe.id, text: value });
  }
  render() {
    const { recipe, show } = this.props;
    const { notes, note } = this.state;
    const { stars, madeThis } = recipe;
    const bImage = {
      backgroundImage: recipe.image ? `url(${recipe.image.fields.file.url})` : '',
    };
    const notesStyle = {
      zIndex: notes ? 294 : '',
    };
    const button = {
      color: madeThis ? 'white' : 'black',
      backgroundColor: madeThis ? 'black' : 'white',
    };
    const ingredients = _.map(recipe.ingredients, (i) => {
      if (!i) return '';
      if (i.charAt(0) === '@') return <h6 className="ingredients-separator" key={i}>{i.substring(1)}</h6>;
      const iMap = i.split(/@(.+)/);
      if (!iMap[1]) {
        iMap[1] = iMap[0];
        iMap[0] = '';
      }
      return (
        <div className="ingredients-group" key={i}>
          <span>{iMap[0].trim()}</span>
          <span>{iMap[1].trim()}</span>
        </div>
      );
    });
    const Directions = recipe.directions ? marked(recipe.directions).replace(/<a\s+href=/gi, '<a target="_blank" href=') : '';
    const RecipeBody = (
      <div className="recipe-body">
        <div className="recipe-ingredients">
          <h4 className="recipe-body-title">Ingredients</h4>
          <div className="ingredients-list">
            {ingredients}
          </div>
        </div>
        <div className="directions-list">
          <h4 className="recipe-body-title">Directions</h4>
          <div className="directions-content" target="_blank" dangerouslySetInnerHTML={{ __html: Directions }} />
        </div>
      </div>
    );
    const NotesHolder = (
      <div className="notes">
        <h4 className="recipe-body-title">Recipe Notes:</h4>
        <textarea className="notes-box" value={note} onChange={this.handleNotes} />
      </div>
    );
    return (
      <div className="full-recipe">
        <div className="full-recipe-menu">
          <div className="calendar-top" onClick={() => this.setState({ notes: false })}>
            <span className="calendar-day">Recipe</span>
          </div>
          <div className="notes-menu-tab" style={notesStyle} onClick={() => this.setState({ notes: true })}>
            Notes
          </div>
        </div>
        <div className="recipe-holder">
          <div className="recipe-header">
            <div className="recipe-image" style={bImage} />
            <div className="recipe-detail-box">
              <div className="full-recipe-title">{`${recipe.title || ''} ${recipe.subtitle || ''}`}</div>
              <div className="recipe-rating">
                <span className={stars > 4 ? 'give-star' : ''} data-star="5" onClick={this.giveRating}>&#9734;</span>
                <span className={stars > 3 ? 'give-star' : ''} data-star="4" onClick={this.giveRating}>&#9734;</span>
                <span className={stars > 2 ? 'give-star' : ''} data-star="3" onClick={this.giveRating}>&#9734;</span>
                <span className={stars > 1 ? 'give-star' : ''} data-star="2" onClick={this.giveRating}>&#9734;</span>
                <span className={stars > 0 ? 'give-star' : ''} data-star="1" onClick={this.giveRating}>&#9734;</span>
              </div>
              <button className="made-this" style={button} onClick={this.madeThis}>I made this</button>
              <button className="share-button" onClick={() => window.print()}>Print</button>
              <X className="close-tab" onClick={() => show(false)} />
            </div>
          </div>
          { notes ? NotesHolder : RecipeBody }
        </div>
      </div>
    );
  }
}

RecipeShow.propTypes = {
  recipe: PropTypes.object,
  show: PropTypes.func,
};

RecipeShow.defaultProps = {
  recipe: {},
};
