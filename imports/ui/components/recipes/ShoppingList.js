import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import _ from 'underscore';
import { Meteor } from 'meteor/meteor';
import Loading from '/imports/ui/components/general/Loading.js';
import { createClient } from 'contentful';
import { X, Printer } from 'react-feather';
import { getShoppingList } from '/imports/api/swaps/methods.js'

export default class ShoppingList extends React.Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.print = this.print.bind(this);
    this.state = {
      loading: true,
    };
  }
  componentWillMount() {
    const recipe = document.getElementsByClassName('full-recipe')[0];
    if (recipe) recipe.classList.add('noprint');
    const { weekNumber } = this.props;
    const self = this;
    getShoppingList.call({ weekNumber }, (err, res) => {
      self.setState({
        items: res,
        loading: false,
      });
    });
  }
  componentDidMount() {
    document.body.style.overflow = 'hidden';
  }
  componentWillUnmount() {
    const recipe = document.getElementsByClassName('full-recipe')[0];
    if (recipe) recipe.classList.remove('noprint');
    document.body.style.overflow = 'auto';
  }
  close(e) {
    e.preventDefault();
    document.getElementById('popup-mount').style.display = 'none';
    ReactDOM.unmountComponentAtNode(ReactDOM.findDOMNode(this).parentNode);
  }
  print() {
    const content = document.getElementById("shopping-list-group");
    const pri = document.getElementById("ifmcontentstoprint").contentWindow;
    pri.document.open();
    pri.document.write(content.innerHTML);
    pri.document.close();
    pri.focus();
    pri.print();
  }
  render() {
    const { loading, items } = this.state;
    if (loading) return <div className="shopping-list"><Loading /></div>
    const Items = _.map(items, (q, i) => (
      <div className="item-box" key={i}>
        <input name={i} type="checkbox" className="styled-checkbox" />
        <label htmlFor={i} />
        <span>{i}</span>
        <span>{q}</span>
      </div>
    ));
    return (
      <div className="shopping-list" id="shopping-list-group">
        <iframe id="ifmcontentstoprint" style={{height: '0px', width: '0px', position: 'absolute'}} />
        <div className="action-shopping">
          <Printer className="print-list" onClick={this.print} />
          <X className="close-tab" onClick={this.close} />
        </div>
        <h3 className="shopping-list-header">Shopping List</h3>
        <div className="shopping-list-items">
          {Items}
        </div>
      </div>
    )
  }
}

ShoppingList.propTypes = {
  weekNumber: PropTypes.number,
};
