import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { ServiceConfiguration } from 'meteor/service-configuration';

(function () {
  Accounts.emailTemplates.siteName = 'Pregnancy Plate';
  Accounts.emailTemplates.from = 'Pregnancy Plate <subscriptions@pregnancyplate.com>';
  Accounts.urls.resetPassword = function reset(token) {
    return Meteor.absoluteUrl('reset-password/' + token);
  };
  Accounts.urls.verifyEmail = function verify(token) {
    return Meteor.absoluteUrl('verify-email/' + token);
  };
  Accounts.urls.enrollAccount = function enroll(token) {
    return Meteor.absoluteUrl('enroll-account/' + token);
  };
})();

Accounts.onCreateUser((options, user) => {
  const newUser = user;

  if (options.profile) {
    newUser.profile = options.profile;
  }

  newUser.status = 'active';

  const switchCase = options.membership.title.match(/\d+/g)[0];
  if (options.membership) {
    newUser.membership = options.membership;
    switch (Number(switchCase)) {
      case 2:
        newUser.expDate = new Date(Date.now() + (14 * 24 * 60 * 60 * 1000)).toString();
        break;
      case 1:
        newUser.expDate = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000)).toString();
        break;
      case 3:
        newUser.expDate = new Date(Date.now() + (90 * 24 * 60 * 60 * 1000)).toString();
        break;
      case 6:
        newUser.expDate = new Date(Date.now() + (180 * 24 * 60 * 60 * 1000)).toString();
        break;
      default:
        newUser.expDate = null;
    }
  }
  return newUser;
});

Meteor.startup(() => {
  // Accounts.emailTemplates.from = 'Name <name@email.io>';
  Accounts.emailTemplates.siteName = 'Pregnancy Plate';
  Accounts.emailTemplates.from = 'Pregnancy Plate <subscriptions@pregnancyplate.com>';
  ServiceConfiguration.configurations.upsert(
    { service: 'google' },
    {
      $set: {
        clientId: Meteor.settings.GOOGLE_CLIENT_ID,
        loginStyle: 'popup',
        secret: Meteor.settings.GOOGLE_CLIENT_SECRET,
      },
    },
  );
  ServiceConfiguration.configurations.upsert(
    { service: 'facebook' },
    {
      $set: {
        appId: Meteor.settings.FACEBOOK_CLIENT_ID,
        loginStyle: 'popup',
        secret: Meteor.settings.FACEBOOK_CLIENT_SECRET,
      },
    },
  );
});
