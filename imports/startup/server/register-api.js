// Users
import '/imports/api/users/users.js';
import '/imports/api/users/helpers.js';
import '/imports/api/users/methods.js';
import '/imports/api/users/server/publications.js';

// Ratings
import '/imports/api/ratings/ratings.js';
import '/imports/api/ratings/methods.js';
import '/imports/api/ratings/server/publications.js';

// Swaps
import '/imports/api/swaps/swaps.js';
import '/imports/api/swaps/methods.js';
import '/imports/api/swaps/server/publications.js';

// Notes
import '/imports/api/notes/notes.js';
import '/imports/api/notes/methods.js';

// Coupons
import '/imports/api/coupons/coupons.js';
import '/imports/api/coupons/methods.js';
