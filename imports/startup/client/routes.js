import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

// Containers & Pages
// Home
import HomePage from '/imports/ui/pages/home/HomePage.js';
// Accounts
import SignUp from '/imports/ui/pages/accounts/SignUp.js';
import Login from '/imports/ui/pages/accounts/Login.js';
import ForgotPassword from '/imports/ui/pages/accounts/ForgotPassword.js';
import ResetPassword from '/imports/ui/pages/accounts/ResetPassword.js';
import PaymentPage from '/imports/ui/pages/accounts/PaymentPage.js';
import CouponPaymentPage from '/imports/ui/pages/accounts/CouponPaymentPage.js';
import AccountPage from '/imports/ui/pages/accounts/AccountPage.js';
import AccountUpgradePage from '/imports/ui/pages/accounts/AccountUpgradePage.js';
// General
import AppContainer from '/imports/ui/containers/AppContainer.js';
// In App
import DashboardContainer from '/imports/ui/containers/app/DashboardContainer.js';
import AllRecipesContainer from '/imports/ui/containers/app/AllRecipesContainer.js';
// Legal
import PrivacyPage from '/imports/ui/pages/legal/PrivacyPage.js';
import TermsPage from '/imports/ui/pages/legal/TermsPage.js';
import DisclaimersPage from '/imports/ui/pages/legal/DisclaimersPage.js';
// Admin
import AdminContainer from '/imports/ui/containers/AdminContainer.js';
import UsersAdminContainer from '/imports/ui/containers/UsersAdminContainer.js';
import RecipeStatsPage from '/imports/ui/pages/admin/RecipeStatsPage.js';

// Routes
export const renderRoutes = () => (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
    <Route path="/" component={AppContainer}>
      <IndexRoute name="Home" component={HomePage} />
      <Route name="Sign Up" path="/signup" component={SignUp} />
      <Route name="Log In" path="/login" component={Login} />
      <Route name="Forgot Password" path="/forgotpassword" component={ForgotPassword} />
      <Route name="Reset Password" path="/reset-password/:token" component={ResetPassword} />
      <Route name="Coupon Payment Page" path="/coupons/:code" component={CouponPaymentPage} />
      <Route name="Privacy Page" path="/privacy" component={PrivacyPage} />
      <Route name="Terms" path="/terms" component={TermsPage} />
      <Route name="Disclaimers" path="/disclaimers" component={DisclaimersPage} />
      <Route name="Payment" path="/payment" component={PaymentPage} />
      <Route name="Dashboard" path="/dashboard" component={DashboardContainer} />
      <Route name="Browse All Recipes" path="/browseall" component={AllRecipesContainer} />
      <Route name="Manage Account" path="/account" component={AccountPage} />
      <Route name="Account Upgrade" path="/account-upgrade" component={AccountUpgradePage} />
    </Route>
    <Route path="/manager" component={AdminContainer}>
      <IndexRoute component={UsersAdminContainer} />
      <Route path="/manager/recipes" component={RecipeStatsPage} />
    </Route>
  </Router>
);
